package lu.uni.lcsb.minerva.net.service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import lu.uni.lcsb.minerva.net.configuration.ConfigurationService;
import lu.uni.lcsb.minerva.net.entity.ErrorReport;

@Service
@Transactional
public class GitlabServiceImpl implements GitlabService {

  private ConfigurationService config;

  @Autowired
  public GitlabServiceImpl(final ConfigurationService config) {
    this.config = config;
  }

  @Override
  public void sendReport(final ErrorReport report) throws IOException, InterruptedException {
    var description = report.generateReport();
    description = description.replaceAll("\\n", "\n\n");
    var gitlabReport = new GitlabReport("MINERVANET - Error Report " + report.getId(), description);
    var httpClient = HttpClient.newHttpClient();
    var body = gitlabReport.toJSON();
    var request = HttpRequest.newBuilder()
        .header("PRIVATE-TOKEN", config.getGitlabToken())
        .header("Accept", "application/json")
        .header("Content-type", "application/json")
        .uri(URI.create(config.getGitlabUri()))
        .POST(HttpRequest.BodyPublishers.ofString(body))
        .build();
    var response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
    if (response.statusCode() != HttpStatus.CREATED.value()) {
      throw new RuntimeException(response.body().toString());
    }
  }
}
