package lu.uni.lcsb.minerva.net.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import lu.uni.lcsb.minerva.net.entity.MinervaMachine;
import lu.uni.lcsb.minerva.net.entity.MinervaProject;

public interface IMinervaProjectDao extends JpaRepository<MinervaProject, Integer> {

  List<MinervaProject> getByProjectIdAndMachine(String projectId, MinervaMachine machine);

  List<MinervaProject> getByMachine(MinervaMachine machine);

  Page<MinervaProject> findAllByMachine(MinervaMachine machine, Pageable pageable);
}
