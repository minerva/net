package lu.uni.lcsb.minerva.net.service;

public class ServiceNotAvailableException extends Exception {

  public ServiceNotAvailableException(final Exception e) {
    super(e);
  }

  public ServiceNotAvailableException() {
  }

  public ServiceNotAvailableException(final String string) {
    super(string);
  }

  public ServiceNotAvailableException(final String string, final Exception e) {
    super(string, e);
  }

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

}
