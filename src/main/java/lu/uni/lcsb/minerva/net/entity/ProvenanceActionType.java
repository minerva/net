package lu.uni.lcsb.minerva.net.entity;

public enum ProvenanceActionType {
  CREATE,
  DELETE,
  MODIFY
}
