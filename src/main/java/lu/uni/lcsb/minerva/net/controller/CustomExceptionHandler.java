package lu.uni.lcsb.minerva.net.controller;

import java.time.Instant;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

  @Override
  public ResponseEntity<Object> handleMethodArgumentNotValid(
      final MethodArgumentNotValidException ex,
      final HttpHeaders headers,
      final HttpStatus status,
      final WebRequest request) {
    StringBuilder details = new StringBuilder();
    for (ObjectError error : ex.getAllErrors()) {
      if (error instanceof FieldError) {
        details.append("[" + ((FieldError) error).getField() + " - " + error.getDefaultMessage() + "]");
      } else {
        details.append(error.toString());
      }
    }
    return handleExceptionInternal(ex,
        new ReportErrorResponse("Bad request", details.toString(), Instant.now().toString()),
        new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
  }

  @ExceptionHandler(value = { ObjectExistsException.class })
  public ResponseEntity<Object> handleException(final ObjectExistsException ex, final WebRequest request) {
    return handleExceptionInternal(ex,
        new ReportErrorResponse("Object exists", ex.getMessage(), Instant.now().toString()),
        new HttpHeaders(), HttpStatus.CONFLICT, request);
  }

  @ExceptionHandler(value = { ObjectNotFoundException.class })
  public ResponseEntity<Object> handleException(final ObjectNotFoundException ex, final WebRequest request) {
    return handleExceptionInternal(ex,
        new ReportErrorResponse("Object does not exist", ex.getMessage(), Instant.now().toString()),
        new HttpHeaders(), HttpStatus.NOT_FOUND, request);
  }

  @ExceptionHandler(value = { BadQueryException.class })
  public ResponseEntity<Object> handleException(final BadQueryException ex, final WebRequest request) {
    return handleExceptionInternal(ex,
        new ReportErrorResponse("Bad request", ex.getMessage(), Instant.now().toString()),
        new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
  }

  @Override
  protected ResponseEntity<Object> handleExceptionInternal(
      final Exception ex, @Nullable final Object body, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
    if (HttpStatus.INTERNAL_SERVER_ERROR.equals(status)) {
      ex.printStackTrace();
    }
    return super.handleExceptionInternal(ex, body, headers, status, request);
  }
}
