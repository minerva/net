package lu.uni.lcsb.minerva.net.controller;

public class ObjectExistsException extends QueryException {

  public ObjectExistsException(final String string) {
    super(string);
  }

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

}
