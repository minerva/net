package lu.uni.lcsb.minerva.net.service;

import lu.uni.lcsb.minerva.net.entity.Provenance;

public interface ProvenanceService {

  Provenance save(final Provenance provenance);

  long count();

  void deleteAll();
}
