package lu.uni.lcsb.minerva.net.controller.serialization;

import java.io.IOException;

import org.springframework.boot.jackson.JsonComponent;
import org.springframework.data.domain.Sort;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

@JsonComponent
public class OrderSerializer extends StdSerializer<Sort.Order> {
  public OrderSerializer() {
    super(Sort.Order.class);
  }

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  @Override
  public void serialize(final Sort.Order value, final JsonGenerator gen, final SerializerProvider serializers)
      throws IOException {
    gen.writeStartObject();

    gen.writeStringField("property", value.getProperty());
    gen.writeStringField("direction", value.getDirection().name());
    gen.writeEndObject();
  }
}
