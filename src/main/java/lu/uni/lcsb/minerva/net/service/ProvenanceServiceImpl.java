package lu.uni.lcsb.minerva.net.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lu.uni.lcsb.minerva.net.dao.IProvenanceDao;
import lu.uni.lcsb.minerva.net.entity.Provenance;

@Service
public class ProvenanceServiceImpl implements ProvenanceService {

  protected Logger logger = LogManager.getLogger();

  private IProvenanceDao provenanceDao;

  @Autowired
  public ProvenanceServiceImpl(final IProvenanceDao provenanceDao) {
    this.provenanceDao = provenanceDao;
  }

  @Override
  public Provenance save(final Provenance provenance) {
    return provenanceDao.save(provenance);
  }

  @Override
  public long count() {
    return provenanceDao.count();
  }

  @Override
  public void deleteAll() {
    provenanceDao.deleteAll();
  }
}
