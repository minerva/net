package lu.uni.lcsb.minerva.net.service;

import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import lu.uni.lcsb.minerva.net.dao.IMinervaMachineDao;
import lu.uni.lcsb.minerva.net.entity.MinervaMachine;

@Service
public class UserServiceImpl implements UserService {

  protected Logger logger = LogManager.getLogger();

  private IMinervaMachineDao minervaMachineDao;

  @Autowired
  public UserServiceImpl(final IMinervaMachineDao minervaMachineDao) {
    this.minervaMachineDao = minervaMachineDao;
  }

  @Override
  public Optional<User> findByToken(final String token) {
    Optional<MinervaMachine> data = minervaMachineDao.findByAuthenticationToken(token);
    if (data.isPresent()) {
      MinervaMachine machine = data.get();
      User user = new User(machine.getName(), machine.getAuthenticationToken(), true, true, true, true,
          AuthorityUtils.createAuthorityList("MINERVA_MACHINE:" + machine.getId()));
      return Optional.of(user);
    }
    return Optional.empty();
  }
}
