package lu.uni.lcsb.minerva.net.controller;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lu.uni.lcsb.minerva.net.configuration.ConfigurationService;

@RestController
@RequestMapping("/proxy")
public class ProxyController {

  @Autowired
  private ConfigurationService configurationService;

  @Autowired
  public ProxyController(final ConfigurationService configurationService) {
    this.configurationService = configurationService;
  }

  @GetMapping
  public HttpEntity<?> reportError(@RequestParam(value = "url", required = true) final String urlQuery,
      final HttpServletResponse response) throws Exception {
    try {
      URL url = new URL(urlQuery);

      if (!configurationService.getProxyDomains().contains(url.getHost())) {
        return new ResponseEntity<>("Proxy is not allowed for specified domain", HttpStatus.BAD_REQUEST);
      }

      HttpURLConnection connection = (HttpURLConnection) url.openConnection();
      connection.setRequestMethod("GET");
      connection.connect();

      int code = connection.getResponseCode();
      InputStream stream = null;
      if (200 <= code && code <= 299) {
        stream = connection.getInputStream();
      } else {
        stream = connection.getErrorStream();
      }
      IOUtils.copy(stream, response.getOutputStream());
      stream.close();

      return new ResponseEntity<>(HttpStatus.valueOf(code));

    } catch (MalformedURLException e) {
      return new ResponseEntity<>("Invalid url", HttpStatus.BAD_REQUEST);
    }

  }

}
