package lu.uni.lcsb.minerva.net.cron;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import lu.uni.lcsb.minerva.net.configuration.ConfigurationService;
import lu.uni.lcsb.minerva.net.entity.MinervaMachine;
import lu.uni.lcsb.minerva.net.entity.MinervaProject;
import lu.uni.lcsb.minerva.net.entity.Status;
import lu.uni.lcsb.minerva.net.service.MinervaMachineService;
import lu.uni.lcsb.minerva.net.service.MinervaProjectService;
import lu.uni.lcsb.minerva.net.service.ServiceNotAvailableException;

@Component
public class MachineProjectValidationBean {

  private Logger logger = LogManager.getLogger();

  @Autowired
  private MinervaMachineService minervaMachineService;

  @Autowired
  private MinervaProjectService minervaProjectService;

  @Autowired
  private ConfigurationService config;

  @Autowired
  private JavaMailSender mailSender;

  @Scheduled(cron = "0 15 2 * * ?")
  public void validateMachinesAndProjects() {
    try {
      logger.info("Validation of machines and projects data");
      List<MinervaMachine> failedMachines = new ArrayList<>();
      Map<MinervaMachine, List<MinervaProject>> failedProjects = new HashMap<>();
      List<MinervaMachine> list = minervaMachineService.findAll(Pageable.unpaged()).getContent();
      for (MinervaMachine machine : list) {
        try {
          minervaMachineService.fillFromRemote(machine);
          minervaMachineService.update(machine);
          List<MinervaProject> projects = getProblematicProjects(machine);
          if (projects.size() > 0) {
            failedProjects.put(machine, projects);
          }
        } catch (ServiceNotAvailableException e) {
          failedMachines.add(machine);
        }
      }
      if (failedMachines.size() > 0 || failedProjects.size() > 0) {
        sendEmailWithProblems(failedMachines, failedProjects);
      }

    } catch (Exception e) {
      logger.error("Problem with validation", e);
    } finally {
      logger.info("Validation of machines and projects data finished");
    }
  }

  private void sendEmailWithProblems(final List<MinervaMachine> failedMachines, final Map<MinervaMachine, List<MinervaProject>> failedProjects)
      throws MessagingException {
    MimeMessage mail = mailSender.createMimeMessage();
    MimeMessageHelper helper = new MimeMessageHelper(mail, true);

    String content = "<html><body>";
    if (failedMachines.size() > 0) {
      content += createMachineProblemContent(failedMachines);
    }
    if (failedProjects.size() > 0) {
      content += createProjectsProblemContent(failedProjects);
    }
    content += "</body></html>";

    helper.setFrom(config.getMailSender());
    helper.setTo(config.getMailRecipient());
    helper.setSubject("MINERVANET - Project issues ");
    helper.setText(content, true);
    mailSender.send(mail);

  }

  private String createProjectsProblemContent(final Map<MinervaMachine, List<MinervaProject>> failedProjects) {
    String cellStyle = "padding: 8px; line-height: 1.42857143; vertical-align: top; "
        + "font-size: 14px; font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;";

    String result = "<h3>Problem with projects on working minerva machines:</h3>";
    result += "<table style='border: 1px solid #f4f4f4;border-spacing: 0;border-collapse: collapse;'>";
    result += "<thead><th>URL</th><th>projectId</th><th>status</th><th>contact person</th></thead>";

    boolean even = false;
    for (MinervaMachine minervaMachine : failedProjects.keySet()) {
      for (MinervaProject project : failedProjects.get(minervaMachine)) {
        String projectId = project.getProjectId();
        String url = minervaMachine.getRootUrl() + "/?" + "id=" + projectId;
        String rowStyle = "";
        even = !even;
        if (even) {
          rowStyle = " background-color: #f9f9f9;";
        }

        result += "<tr style=\"" + rowStyle + "\">";
        result += "<td style=\"" + cellStyle + "\"><a href=\"" + url + "\">" + url + "</a></td>";
        result += "<td style=\"" + cellStyle + "\">" + projectId + "</td>";
        result += "<td style=\"" + cellStyle + "\">" + project.getStatus() + "</td>";
        result += "<td style=\"" + cellStyle + "\">" + minervaMachine.getName() + " (" + minervaMachine.getEmail() + ")</td>";
        result += "</tr>";
      }
    }
    result += "</table>";
    return result;
  }

  private String createMachineProblemContent(final List<MinervaMachine> failedMachines) {
    String cellStyle = "padding: 8px; line-height: 1.42857143; vertical-align: top; "
        + "font-size: 14px; font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;";

    String result = "<h3>Following minerva machines cannot be connected to</h3>";
    result += "<table style='border: 1px solid #f4f4f4;border-spacing: 0;border-collapse: collapse;'>";
    result += "<thead><th>URL</th><th>minerva version</th><th>contact person</th></thead>";

    boolean even = false;
    for (MinervaMachine minervaMachine : failedMachines) {
      String rowStyle = "";
      even = !even;
      if (even) {
        rowStyle = " background-color: #f9f9f9;";
      }

      result += "<tr style=\"" + rowStyle + "\">";
      result += "<td style=\"" + cellStyle + "\"><a href=\"" + minervaMachine.getRootUrl() + "\">" + minervaMachine.getRootUrl() + "</a></td>";
      result += "<td style=\"" + cellStyle + "\">" + minervaMachine.getVersion() + "</td>";
      result += "<td style=\"" + cellStyle + "\">" + minervaMachine.getName() + " (" + minervaMachine.getEmail() + ")</td>";
      result += "</tr>";
    }
    result += "</table>";
    return result;
  }

  private List<MinervaProject> getProblematicProjects(final MinervaMachine machine) {
    List<MinervaProject> result = new ArrayList<>();

    List<MinervaProject> projects = minervaProjectService.getByMinervaMachine(machine);
    for (MinervaProject project : projects) {
      try {
        minervaProjectService.fillFromRemote(project);
        minervaProjectService.save(project);
      } catch (ServiceNotAvailableException e) {
        result.add(project);
        minervaProjectService.save(project);
      }
    }
    return result;
  }

  @Scheduled(cron = "0 15 2 * * MON")
  public void summaryMachinesAndProjects() {
    try {
      logger.info("Generating summary");
      List<MinervaMachine> machines = minervaMachineService.findAll(Pageable.unpaged()).getContent();
      List<MinervaProject> projects = minervaProjectService.findAll(Pageable.unpaged()).getContent();
      sendEmailWithSummary(machines, projects);

    } catch (Exception e) {
      logger.error("Problem with validation", e);
    } finally {
      logger.info("Generating summary finished");
    }
  }

  private void sendEmailWithSummary(final List<MinervaMachine> machines, final List<MinervaProject> projects) throws MessagingException {
    int validProjects = 0;
    for (MinervaProject minervaProject : projects) {
      if (minervaProject.getStatus() == Status.OK) {
        validProjects++;
      }
    }
    MimeMessage mail = mailSender.createMimeMessage();
    MimeMessageHelper helper = new MimeMessageHelper(mail, true);

    String content = "<html><body><p>Minerva net statistics:";
    content += "<ul>";
    content += "<li>" + machines.size() + "</b> machines registered</li>";
    content += "<li>" + projects.size() + " shared projects</li>";
    content += "<li>" + validProjects + " shared projects are valid</li>";
    content += "</ul>";
    content += "Full list can be found <a href=\"https://minerva-net.lcsb.uni.lu/\">here</a>.";
    content += "</body></html>";

    helper.setFrom(config.getMailSender());
    helper.setTo(config.getMailRecipient());
    helper.setSubject("MINERVANET - Projects summary");
    helper.setText(content, true);
    mailSender.send(mail);

  }

}
