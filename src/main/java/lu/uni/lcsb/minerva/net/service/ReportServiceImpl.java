package lu.uni.lcsb.minerva.net.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import lu.uni.lcsb.minerva.net.configuration.ConfigurationService;
import lu.uni.lcsb.minerva.net.dao.IErrorReportDao;
import lu.uni.lcsb.minerva.net.entity.ErrorReport;

@Service
@Transactional
public class ReportServiceImpl implements ReportService {

  private IErrorReportDao errorReportDao;
  private JavaMailSender mailSender;
  private ConfigurationService config;

  @Autowired
  public ReportServiceImpl(final IErrorReportDao errorReportDao, final JavaMailSender mailSender, final ConfigurationService config) {
    this.errorReportDao = errorReportDao;
    this.mailSender = mailSender;
    this.config = config;
  }

  @Override
  public void saveReport(final ErrorReport report) {
    errorReportDao.save(report);
  }

  @Override
  public void sendEmail(final ErrorReport report) {
    var message = new SimpleMailMessage();
    message.setFrom(config.getMailSender());
    message.setTo(config.getMailRecipient());
    message.setSubject("MINERVANET - Error Report " + report.getId());
    message.setText(report.generateReport());
    mailSender.send(message);
  }

  @Override
  public void deleteAll() {
    errorReportDao.deleteAll();
  }

}
