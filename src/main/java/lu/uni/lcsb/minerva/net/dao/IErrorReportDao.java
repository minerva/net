package lu.uni.lcsb.minerva.net.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import lu.uni.lcsb.minerva.net.entity.ErrorReport;

public interface IErrorReportDao extends JpaRepository<ErrorReport, Integer> {
  
  Optional<ErrorReport> findById(Integer id);
  
  List<ErrorReport> findAll();

}
