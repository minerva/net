package lu.uni.lcsb.minerva.net.configuration;

import java.time.Instant;
import java.util.List;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import lu.uni.lcsb.minerva.net.controller.serialization.InstantDeserializer;
import lu.uni.lcsb.minerva.net.controller.serialization.InstantSerializer;
import lu.uni.lcsb.minerva.net.controller.serialization.OrderSerializer;
import lu.uni.lcsb.minerva.net.controller.serialization.PageSerializer;
import lu.uni.lcsb.minerva.net.controller.serialization.PageableSerializer;
import lu.uni.lcsb.minerva.net.controller.serialization.SortSerializer;

@Configuration
@EnableScheduling
@ComponentScan(basePackages = { "lu.uni.lcsb.minerva.net.controller" })
public class WebConfig implements WebMvcConfigurer {

  @Override
  public void addCorsMappings(final CorsRegistry registry) {
    registry.addMapping("/**").allowedMethods("*");
  }

  @SuppressWarnings({ "unchecked", "rawtypes" })
  @Override
  public void extendMessageConverters(final List<HttpMessageConverter<?>> converters) {
    ObjectMapper mapper = new ObjectMapper();

    SimpleModule module = new SimpleModule();

    module.addSerializer(Instant.class, new InstantSerializer());
    module.addDeserializer(Instant.class, new InstantDeserializer());
    module.addSerializer(Pageable.class, new PageableSerializer());
    module.addSerializer(Page.class, new PageSerializer());
    module.addSerializer(Sort.class, new SortSerializer());
    module.addSerializer(Sort.Order.class, new OrderSerializer());

    mapper.registerModule(module);

    for (HttpMessageConverter<?> converter : converters) {
      if (converter instanceof MappingJackson2HttpMessageConverter) {
        MappingJackson2HttpMessageConverter m = (MappingJackson2HttpMessageConverter) converter;
        m.setObjectMapper(mapper);
      }
    }
  }
}
