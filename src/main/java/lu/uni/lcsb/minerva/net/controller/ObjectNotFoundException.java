package lu.uni.lcsb.minerva.net.controller;

public class ObjectNotFoundException extends QueryException {

  public ObjectNotFoundException(final String string) {
    super(string);
  }

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

}
