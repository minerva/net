package lu.uni.lcsb.minerva.net.controller;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lu.uni.lcsb.minerva.net.entity.MinervaMachine;
import lu.uni.lcsb.minerva.net.entity.MinervaProject;
import lu.uni.lcsb.minerva.net.service.MinervaMachineService;
import lu.uni.lcsb.minerva.net.service.MinervaProjectService;
import lu.uni.lcsb.minerva.net.service.ServiceNotAvailableException;

@RestController
@Validated
@RequestMapping("/api/machines/{id}/projects/")
public class MinervaProjectController {

  private Logger logger = LogManager.getLogger();

  private MinervaMachineService minervaMachineService;
  private MinervaProjectService minervaProjectService;

  @Autowired
  public MinervaProjectController(final MinervaMachineService minervaMachineService, final MinervaProjectService minervaProjectService) {
    this.minervaMachineService = minervaMachineService;
    this.minervaProjectService = minervaProjectService;
  }

  static class MinervaProjectPostData {
    @NotBlank
    private String projectId;

    @Email
    private String email;

    public String getEmail() {
      return email;
    }

    public void setEmail(final String email) {
      this.email = email;
    }

    public String getProjectId() {
      return projectId;
    }

    public void setProjectId(final String projectId) {
      this.projectId = projectId;
    }

    public MinervaProject toMinervaProject(final MinervaMachine machine) {
      MinervaProject result = new MinervaProject();
      result.setMachine(machine);
      result.setProjectId(projectId);
      result.setContactEmail(email);
      return result;
    }

  }

  @PostMapping
  @PreAuthorize("hasAnyAuthority('MINERVA_MACHINE:' + #machineId)")
  public MinervaProject create(@PathVariable(name = "id") final int machineId, @Valid @RequestBody final MinervaProjectPostData input)
      throws QueryException {
    MinervaMachine machine = minervaMachineService.getById(machineId);
    if (machine == null) {
      throw new ObjectNotFoundException("Machine does not exist");
    }
    MinervaProject project = minervaProjectService.getByProjectId(machine, input.getProjectId());
    if (project != null) {
      throw new ObjectExistsException("Project already exists");
    }
    project = input.toMinervaProject(machine);
    try {
      minervaProjectService.fillFromRemote(project);
    } catch (ServiceNotAvailableException e) {
      throw new BadQueryException("Minerva project not available: " + e.getMessage(), e);
    }
    minervaProjectService.save(project);
    return project;
  }

  @DeleteMapping("/{projectId}")
  @PreAuthorize("hasAnyAuthority('MINERVA_MACHINE:' + #machineId)")
  public void delete(
      @NotNull @PathVariable(name = "id") final int machineId,
      @NotNull @PathVariable(name = "projectId") final String projectId)
      throws ObjectNotFoundException {
    MinervaMachine machine = minervaMachineService.getById(machineId);
    MinervaProject project = minervaProjectService.getByProjectId(machine, projectId);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given id does not exist");
    }
    minervaProjectService.delete(project);
  }

  @GetMapping
  public Page<MinervaProject> list(@PathVariable(name = "id") final int machineId, final Pageable pageable) {
    MinervaMachine machine = minervaMachineService.getById(machineId);
    return minervaProjectService.findAllByMinervaMachine(machine, pageable);
  }

}
