package lu.uni.lcsb.minerva.net.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class GitlabReport {

  private String title;

  private String description;

  private static final String labels = "BUG";
  private static final String confidential = "true";

  public GitlabReport(final String title, final String description) {
    this.title = title;
    this.description = description;
  }

  public String toJSON() throws JsonProcessingException {
    var objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
    return objectWriter.writeValueAsString(this);
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(final String title) {
    this.title = title;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(final String description) {
    this.description = description;
  }

  public String getLabels() {
    return labels;
  }

  public String getConfidential() {
    return confidential;
  }
}
