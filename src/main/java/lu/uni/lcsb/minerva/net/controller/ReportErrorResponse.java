package lu.uni.lcsb.minerva.net.controller;

public class ReportErrorResponse {

  private String message;
  private String timestamp;
  private String details;

  ReportErrorResponse(final String message, final String details, final String timestamp) {
    this.message = message;
    this.details = details;
    this.timestamp = timestamp;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(final String message) {
    this.message = message;
  }

  public String getDetails() {
    return details;
  }

  public void setDetails(final String details) {
    this.details = details;
  }

  public String getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(final String timestamp) {
    this.timestamp = timestamp;
  }
}
