package lu.uni.lcsb.minerva.net.service;

import javax.validation.constraints.NotNull;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import lu.uni.lcsb.minerva.net.entity.MinervaMachine;

public interface MinervaMachineService {

  MinervaMachine add(final MinervaMachine machine);

  void checkRemoteAccess(final MinervaMachine machine);

  void fillFromRemote(final MinervaMachine machine) throws ServiceNotAvailableException;

  void deleteAll();

  MinervaMachine getByUrl(String rootUrl);

  long count();

  MinervaMachine getById(@NotNull int id);

  void delete(MinervaMachine machine);

  Page<MinervaMachine> findAll(Pageable pageable);

  MinervaMachine update(MinervaMachine machine);
}
