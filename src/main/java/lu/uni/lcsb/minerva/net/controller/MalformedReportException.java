package lu.uni.lcsb.minerva.net.controller;

public class MalformedReportException extends RuntimeException {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public MalformedReportException() {
  }

  public MalformedReportException(final String message) {
    super(message);
  }

  public MalformedReportException(final String message, final Throwable cause) {
    super(message, cause);
  }

  public MalformedReportException(final Throwable cause) {
    super(cause);
  }

  public MalformedReportException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

}
