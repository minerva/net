package lu.uni.lcsb.minerva.net.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import lu.uni.lcsb.minerva.net.entity.MinervaMachine;

public interface IMinervaMachineDao extends JpaRepository<MinervaMachine, Integer> {

  @Override
  Optional<MinervaMachine> findById(Integer id);

  @Override
  List<MinervaMachine> findAll();

  List<MinervaMachine> getByRootUrl(String rootUrl);

  Optional<MinervaMachine> findByAuthenticationToken(String token);

}
