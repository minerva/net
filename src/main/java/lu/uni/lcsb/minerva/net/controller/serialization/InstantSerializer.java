package lu.uni.lcsb.minerva.net.controller.serialization;

import java.io.IOException;
import java.time.Instant;

import org.springframework.boot.jackson.JsonComponent;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

@JsonComponent
public class InstantSerializer extends StdSerializer<Instant> {

  public InstantSerializer() {
    super(Instant.class);
  }

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  @Override
  public void serialize(
      final Instant value, final JsonGenerator jgen, final SerializerProvider provider)
      throws IOException, JsonProcessingException {
    if (value == null) {
      jgen.writeNull();
    } else {
      jgen.writeString(value.toString());
    }
  }
}