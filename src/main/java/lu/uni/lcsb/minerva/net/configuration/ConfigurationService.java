package lu.uni.lcsb.minerva.net.configuration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

@Service
@PropertySource("classpath:minervanet.properties")
@PropertySource(value = "file:/etc/minervanet.properties", ignoreResourceNotFound = true)
public class ConfigurationService {

  /*
   * ------------------ PERSISTENCE CONFIG ------------------
   */

  @Value("${db.host}")
  private String dbHost;

  @Value("${db.port}")
  private int dbPort;

  @Value("${db.name}")
  private String dbName;

  @Value("${db.user}")
  private String dbUser;

  @Value("${db.pass}")
  private String dbPass;

  /*
   * ------------------ MAIL CONFIG ------------------
   */

  @Value("${mail.host}")
  private String mailHost;

  @Value("${mail.port}")
  private int mailPort;

  @Value("${mail.user}")
  private String mailUser;

  @Value("${mail.pass}")
  private String mailPass;

  @Value("${mail.from}")
  private String mailSender;

  @Value("${mail.to}")
  private String mailRecipient;

  /*
   * ------------------ GITLAB CONFIG ------------------
   */

  @Value("${gitlab.token}")
  private String gitlabToken;

  @Value("${gitlab.uri}")
  private String gitlabUri;

  /*
   * ------------------ PROXY CONFIG ------------------
   */
  @Value("${proxy.domains}")
  private String proxyDomains;

  public String getDbHost() {
    return dbHost;
  }

  public void setDbHost(final String dbHost) {
    this.dbHost = dbHost;
  }

  public int getDbPort() {
    return dbPort;
  }

  public void setDbPort(final int dbPort) {
    this.dbPort = dbPort;
  }

  public String getDbName() {
    return dbName;
  }

  public void setDbName(final String dbName) {
    this.dbName = dbName;
  }

  public String getDbUser() {
    return dbUser;
  }

  public void setDbUser(final String dbUser) {
    this.dbUser = dbUser;
  }

  public String getDbPass() {
    return dbPass;
  }

  public void setDbPass(final String dbPass) {
    this.dbPass = dbPass;
  }

  public String getMailHost() {
    return mailHost;
  }

  public void setMailHost(final String mailHost) {
    this.mailHost = mailHost;
  }

  public int getMailPort() {
    return mailPort;
  }

  public void setMailPort(final int mailPort) {
    this.mailPort = mailPort;
  }

  public String getMailUser() {
    return mailUser;
  }

  public void setMailUser(final String mailUser) {
    this.mailUser = mailUser;
  }

  public String getMailPass() {
    return mailPass;
  }

  public void setMailPass(final String mailPass) {
    this.mailPass = mailPass;
  }

  public String getMailSender() {
    return mailSender;
  }

  public void setMailSender(final String mailSender) {
    this.mailSender = mailSender;
  }

  public String getMailRecipient() {
    return mailRecipient;
  }

  public void setMailRecipient(final String mailRecipient) {
    this.mailRecipient = mailRecipient;
  }

  public String getGitlabToken() {
    return gitlabToken;
  }

  public void setGitlabToken(final String gitlabToken) {
    this.gitlabToken = gitlabToken;
  }

  public String getGitlabUri() {
    return gitlabUri;
  }

  public void setGitlabUri(final String gitlabUri) {
    this.gitlabUri = gitlabUri;
  }

  public List<String> getProxyDomains() {
    if (proxyDomains == null) {
      return new ArrayList<>();
    }
    return Arrays.asList(proxyDomains.split(","));
  }
}
