package lu.uni.lcsb.minerva.net.entity;

import java.time.Instant;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "projectId", "machine_id" }))
public class MinervaProject {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE)
  @JsonIgnore
  private int id;

  @NotNull
  @ManyToOne(optional = false)
  private MinervaMachine machine;

  @NotNull
  private String projectId;

  @Email
  @JsonIgnore
  private String contactEmail;

  @NotNull
  private String mapName;

  @NotNull
  private String mapVersion;

  @NotNull
  private String organism;

  @NotNull
  private String disease;

  @NotNull
  private String license;

  @NotNull
  private Status status;

  @NotNull
  private Instant statusUpdatedAt;

  @NotNull
  private Instant createdAt = Instant.now();

  public MinervaMachine getMachine() {
    return machine;
  }

  public void setMachine(final MinervaMachine machine) {
    this.machine = machine;
  }

  public String getProjectId() {
    return projectId;
  }

  public void setProjectId(final String projectId) {
    this.projectId = projectId;
  }

  public String getMapName() {
    return mapName;
  }

  public void setMapName(final String mapName) {
    this.mapName = mapName;
  }

  public String getMapVersion() {
    return mapVersion;
  }

  public void setMapVersion(final String mapVersion) {
    this.mapVersion = mapVersion;
  }

  public String getOrganism() {
    return organism;
  }

  public void setOrganism(final String organism) {
    this.organism = organism;
  }

  public String getDisease() {
    return disease;
  }

  public void setDisease(final String disease) {
    this.disease = disease;
  }

  public Status getStatus() {
    return status;
  }

  public void setStatus(final Status status) {
    this.status = status;
  }

  public Instant getStatusUpdatedAt() {
    return statusUpdatedAt;
  }

  public void setStatusUpdatedAt(final Instant statusUpdatedAt) {
    this.statusUpdatedAt = statusUpdatedAt;
  }

  public Instant getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(final Instant createdAt) {
    this.createdAt = createdAt;
  }

  public String getContactEmail() {
    return contactEmail;
  }

  public void setContactEmail(final String contactEmail) {
    this.contactEmail = contactEmail;
  }

  public int getId() {
    return id;
  }

  public void setId(final int id) {
    this.id = id;
  }

  public String getLicense() {
    return license;
  }

  public void setLicense(final String license) {
    this.license = license;
  }
}
