package lu.uni.lcsb.minerva.net.controller;

public abstract class QueryException extends Exception {

  public QueryException(final String string, final Exception e) {
    super(string, e);
  }

  public QueryException(final String string) {
    super(string);
  }

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

}
