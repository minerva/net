package lu.uni.lcsb.minerva.net.configuration;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import lu.uni.lcsb.minerva.net.service.UserService;

@Component
public class AuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

  @Autowired
  private UserService userService;

  @Override
  protected void additionalAuthenticationChecks(final UserDetails userDetails,
      final UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken) throws AuthenticationException {
  }

  @Override
  protected UserDetails retrieveUser(final String userName, final UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken)
      throws AuthenticationException {
    Object token = usernamePasswordAuthenticationToken.getCredentials();
    return Optional
        .ofNullable(token)
        .map(String::valueOf)
        .flatMap(userService::findByToken)
        .orElse(new User("GUEST", "", true, true, true, true, new ArrayList<>()));

  }
}