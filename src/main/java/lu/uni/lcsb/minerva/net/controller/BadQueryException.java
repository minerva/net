package lu.uni.lcsb.minerva.net.controller;

public class BadQueryException extends QueryException {

  public BadQueryException(final String string) {
    super(string);
  }

  public BadQueryException(final String string, final Exception e) {
    super(string, e);
  }

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

}
