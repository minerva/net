package lu.uni.lcsb.minerva.net.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import lu.uni.lcsb.minerva.net.entity.MinervaMachine;
import lu.uni.lcsb.minerva.net.entity.MinervaProject;

public interface MinervaProjectService {

  MinervaProject save(final MinervaProject project);

  void fillFromRemote(final MinervaProject project) throws ServiceNotAvailableException;

  void deleteAll();

  long count();

  MinervaProject getByProjectId(final MinervaMachine machine, String projectId);

  void delete(MinervaProject project);

  Page<MinervaProject> findAll(Pageable pageable);

  List<MinervaProject> getByMinervaMachine(MinervaMachine machine);

  Page<MinervaProject> findAllByMinervaMachine(MinervaMachine machine, Pageable pageable);
}
