package lu.uni.lcsb.minerva.net.entity;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

@Entity
public class ErrorReport {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE)
  private int id;

  private String url;

  private String login;

  private String email;

  private String browser;

  private Instant timestamp;

  @NotBlank
  private String version;

  private String comment;

  @NotBlank
  @Column(columnDefinition = "text")
  private String stacktrace;

  @Column(columnDefinition = "text")
  private String javaStacktrace;

  public String generateReport() {
    final var NO_DATA = "Not provided by reporter";
    var report = String.format("id: %s\n", id);
    report += String.format("url: %s\n", url == null ? NO_DATA : url);
    report += String.format("login: %s\n", login == null ? NO_DATA : login);
    report += String.format("email: %s\n", email == null ? NO_DATA : email);
    report += String.format("browser: %s\n", browser == null ? NO_DATA : browser);
    report += String.format("timestamp: %s\n", timestamp == null ? NO_DATA : timestamp);
    report += String.format("version: %s\n", version == null ? NO_DATA : version);
    report += String.format("comment: %s\n", comment == null ? NO_DATA : comment);
    report += String.format("stacktrace: ```%s```\n", stacktrace == null ? NO_DATA : stacktrace);
    report += String.format("Java stacktrace: ```%s```\n", javaStacktrace == null ? NO_DATA : javaStacktrace);
    return report;
  }

  public int getId() {
    return id;
  }

  public void setId(final int id) {
    this.id = id;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(final String url) {
    this.url = url;
  }

  public String getLogin() {
    return login;
  }

  public void setLogin(final String login) {
    this.login = login;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(final String email) {
    this.email = email;
  }

  public String getBrowser() {
    return browser;
  }

  public void setBrowser(final String browser) {
    this.browser = browser;
  }

  public Instant getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(final Instant timestamp) {
    this.timestamp = timestamp;
  }

  public String getStacktrace() {
    return stacktrace;
  }

  public void setStacktrace(final String stacktrace) {
    this.stacktrace = stacktrace;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(final String version) {
    this.version = version;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(final String comment) {
    this.comment = comment;
  }

  public String getJavaStacktrace() {
    return javaStacktrace;
  }

  public void setJavaStacktrace(final String javaStacktrace) {
    this.javaStacktrace = javaStacktrace;
  }
}
