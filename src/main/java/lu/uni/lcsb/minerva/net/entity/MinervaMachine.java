package lu.uni.lcsb.minerva.net.entity;

import java.time.Instant;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class MinervaMachine {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE)
  private int id;

  @NotBlank
  private String rootUrl;

  @JsonIgnore
  @Email
  private String email;

  @JsonIgnore
  @NotNull
  private String name;

  @JsonIgnore
  private String authenticationToken;

  @NotBlank
  private String version;

  @NotNull
  private Instant createdAt = Instant.now();

  @NotNull
  private Status status;

  @NotNull
  private Instant statusUpdatedAt;

  public String getRootUrl() {
    return rootUrl;
  }

  public void setRootUrl(final String rootUrl) {
    this.rootUrl = rootUrl;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(final String email) {
    this.email = email;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public Instant getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(final Instant createdAt) {
    this.createdAt = createdAt;
  }

  public Status getStatus() {
    return status;
  }

  public void setStatus(final Status status) {
    this.status = status;
  }

  public Instant getStatusUpdatedAt() {
    return statusUpdatedAt;
  }

  public void setStatusUpdatedAt(final Instant statusUpdatedAt) {
    this.statusUpdatedAt = statusUpdatedAt;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(final String version) {
    this.version = version;
  }

  public int getId() {
    return id;
  }

  public void setId(final int id) {
    this.id = id;
  }

  public String getAuthenticationToken() {
    return authenticationToken;
  }

  public void setAuthenticationToken(final String authenticationToken) {
    this.authenticationToken = authenticationToken;
  }

}
