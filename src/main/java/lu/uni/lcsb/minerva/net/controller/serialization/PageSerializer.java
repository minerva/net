package lu.uni.lcsb.minerva.net.controller.serialization;

import java.io.IOException;

import org.springframework.data.domain.Page;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public class PageSerializer<T> extends StdSerializer<Page<T>> {
  public PageSerializer() {
    super(Page.class, false);
  }

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  @Override
  public void serialize(final Page<T> value, final JsonGenerator gen, final SerializerProvider serializers)
      throws IOException {
    gen.writeStartObject();
    gen.writeObjectField("pageContent", value.getContent());
    gen.writeBooleanField("isLastPage", value.isLast());
    gen.writeNumberField("totalPages", value.getTotalPages());
    gen.writeNumberField("totalElements", value.getTotalElements());
    gen.writeNumberField("pageSize", value.getSize());
    gen.writeNumberField("pageNumber", value.getNumber());
    gen.writeEndObject();
  }
}