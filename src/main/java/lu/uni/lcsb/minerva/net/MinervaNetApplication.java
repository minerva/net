package lu.uni.lcsb.minerva.net;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;

@SpringBootApplication(exclude = HibernateJpaAutoConfiguration.class)
public class MinervaNetApplication {

  public static void main(final String[] args) {
    SpringApplication.run(MinervaNetApplication.class, args);
  }

}