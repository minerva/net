package lu.uni.lcsb.minerva.net.service;

import java.util.Optional;

import org.springframework.security.core.userdetails.User;

public interface UserService {

  public Optional<User> findByToken(String token);
}
