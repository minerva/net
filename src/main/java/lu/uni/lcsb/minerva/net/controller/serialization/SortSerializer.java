package lu.uni.lcsb.minerva.net.controller.serialization;

import java.io.IOException;

import org.springframework.boot.jackson.JsonComponent;
import org.springframework.data.domain.Sort;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

@JsonComponent
public class SortSerializer extends StdSerializer<Sort> {
  public SortSerializer() {
    super(Sort.class);
  }

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  @Override
  public void serialize(final Sort value, final JsonGenerator gen, final SerializerProvider serializers)
      throws IOException {
    gen.writeStartObject();

    gen.writeArrayFieldStart("order");
    for (Sort.Order order : value) {
      gen.writeObject(order);
    }
    gen.writeEndArray();
    gen.writeBooleanField("sorted", value.isSorted());
    gen.writeEndObject();
  }
}