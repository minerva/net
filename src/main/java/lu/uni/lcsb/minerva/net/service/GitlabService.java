package lu.uni.lcsb.minerva.net.service;

import java.io.IOException;

import lu.uni.lcsb.minerva.net.entity.ErrorReport;

public interface GitlabService {
  public void sendReport(final ErrorReport report) throws IOException, InterruptedException;

}
