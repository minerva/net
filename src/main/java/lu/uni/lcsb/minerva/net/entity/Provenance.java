package lu.uni.lcsb.minerva.net.entity;

import java.time.Instant;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class Provenance {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE)
  private int id;

  @NotNull
  private Class<?> objectType;

  @NotNull
  private int objectId;

  @NotNull
  @Enumerated(EnumType.STRING)
  private ProvenanceActionType type;

  @NotBlank
  private String description;

  @NotNull
  private Instant createdAt = Instant.now();

  public Class<?> getObjectType() {
    return objectType;
  }

  public void setObjectType(final Class<?> objectType) {
    this.objectType = objectType;
  }

  public int getObjectId() {
    return objectId;
  }

  public void setObjectId(final int objectId) {
    this.objectId = objectId;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(final String description) {
    this.description = description;
  }

  public ProvenanceActionType getType() {
    return type;
  }

  public void setType(final ProvenanceActionType type) {
    this.type = type;
  }

  public Instant getCreatedAt() {
    return createdAt;
  }

}
