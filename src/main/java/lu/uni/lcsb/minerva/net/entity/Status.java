package lu.uni.lcsb.minerva.net.entity;

public enum Status {
  OK, NOT_AVAILABLE, FORBIDDEN
}
