package lu.uni.lcsb.minerva.net.configuration;

import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.orm.jpa.hibernate.SpringImplicitNamingStrategy;
import org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages = {
    "lu.uni.lcsb.minerva.net.configuration",
    "lu.uni.lcsb.minerva.net.controller",
    "lu.uni.lcsb.minerva.net.dao",
    "lu.uni.lcsb.minerva.net.entity",
    "lu.uni.lcsb.minerva.net.service" })
@EnableJpaRepositories(basePackages = "lu.uni.lcsb.minerva.net.dao", entityManagerFactoryRef = "sessionFactory")
@SpringBootApplication(exclude = HibernateJpaAutoConfiguration.class)
public class ServiceConfig {

  private ConfigurationService config;

  @Autowired
  public ServiceConfig(final ConfigurationService configurationService) {
    this.config = configurationService;
  }

  /*
   * ------------------ PERSISTENCE CONFIG ------------------
   */

  @Bean
  public HikariDataSource dataSource() {
    var dataSource = new HikariDataSource();
    dataSource.setDriverClassName("org.postgresql.Driver");
    dataSource.setJdbcUrl("jdbc:postgresql://" + config.getDbHost() + ":" + config.getDbPort() + "/" + config.getDbName());
    dataSource.setUsername(config.getDbUser());
    dataSource.setPassword(config.getDbPass());
    dataSource.setIdleTimeout(30000);
    dataSource.setMaximumPoolSize(30);
    return dataSource;
  }

  @Bean
  public LocalSessionFactoryBean sessionFactory(final DataSource dataSource) {
    var props = new Properties();
    props.setProperty(Environment.DIALECT, "org.hibernate.dialect.PostgreSQL95Dialect");
    props.setProperty(Environment.SHOW_SQL, "false");
    var factory = new LocalSessionFactoryBean();
    factory.setDataSource(dataSource);
    factory.setPackagesToScan("lu.uni.lcsb.minerva.net.entity");
    factory.setHibernateProperties(props);
    factory.setImplicitNamingStrategy(SpringImplicitNamingStrategy.INSTANCE);
    factory.setPhysicalNamingStrategy(new SpringPhysicalNamingStrategy());
    return factory;
  }

  @Bean
  public HibernateTransactionManager transactionManager(final SessionFactory sessionFactory) {
    var txManager = new HibernateTransactionManager();
    txManager.setSessionFactory(sessionFactory);
    return txManager;
  }

  /*
   * ----------- MAIL CONFIG -----------
   */

  @Bean
  public JavaMailSender javaMailSender() {
    var mailSender = new JavaMailSenderImpl();
    mailSender.setHost(config.getMailHost());
    mailSender.setPort(config.getMailPort());
    mailSender.setUsername(config.getMailUser());
    mailSender.setPassword(config.getMailPass());
    var properties = mailSender.getJavaMailProperties();
    properties.put("mail.transport.protocol", "smtp");
    // properties.put("mail.smtp.auth", "true");
    // properties.put("mail.smtp.starttls.enable", "true");
    properties.put("mail.debug", "true");
    return mailSender;
  }
}
