package lu.uni.lcsb.minerva.net.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import lu.uni.lcsb.minerva.net.entity.MinervaMachine;
import lu.uni.lcsb.minerva.net.entity.MinervaProject;
import lu.uni.lcsb.minerva.net.service.MinervaMachineService;
import lu.uni.lcsb.minerva.net.service.MinervaProjectService;
import lu.uni.lcsb.minerva.net.service.ServiceNotAvailableException;

@RestController
@Validated
@RequestMapping("/api/machines/")
public class MinervaMachineController {

  private Logger logger = LogManager.getLogger();

  private MinervaMachineService minervaMachineService;
  private MinervaProjectService minervaProjectService;

  private ObjectMapper mapper;

  @Autowired
  public MinervaMachineController(final MinervaMachineService minervaMachineService, final ObjectMapper mapper,
      final MinervaProjectService minervaProjectService) {
    this.minervaMachineService = minervaMachineService;
    this.mapper = mapper;
    this.minervaProjectService = minervaProjectService;
  }

  static class MinervaMachinePostData {
    @NotBlank
    private String rootUrl;

    @Email
    @NotNull
    private String email;

    @NotNull
    private String name;

    public String getRootUrl() {
      return rootUrl;
    }

    public void setRootUrl(final String rootUrl) {
      this.rootUrl = rootUrl;
    }

    public String getEmail() {
      return email;
    }

    public void setEmail(final String email) {
      this.email = email;
    }

    public String getName() {
      return name;
    }

    public void setName(final String name) {
      this.name = name;
    }

    public MinervaMachine toMinervaMachine() {
      MinervaMachine result = new MinervaMachine();
      result.setEmail(email);
      result.setRootUrl(rootUrl);
      result.setName(name);
      return result;
    }

  }

  @PostMapping
  public Map<String, Object> create(@Valid @RequestBody final MinervaMachinePostData input) throws QueryException {
    MinervaMachine machine = input.toMinervaMachine();
    if (minervaMachineService.getByUrl(machine.getRootUrl()) != null) {
      throw new ObjectExistsException("Minerva machine is already regsitered");
    }
    try {
      minervaMachineService.fillFromRemote(machine);
    } catch (ServiceNotAvailableException e) {
      throw new BadQueryException("Minerva machine not available: " + e.getMessage(), e);
    }
    minervaMachineService.add(machine);

    Map<String, Object> result = mapper.convertValue(machine, new TypeReference<Map<String, Object>>() {
    });
    result.put("authenticationToken", machine.getAuthenticationToken());
    return result;
  }

  @DeleteMapping("/{id}")
  @PreAuthorize("hasAnyAuthority('MINERVA_MACHINE:' + #id)")
  public void delete(@NotNull @PathVariable(name = "id") final int id) throws ObjectNotFoundException {
    MinervaMachine machine = minervaMachineService.getById(id);
    if (machine == null) {
      throw new ObjectNotFoundException("Machine with given id does not exist");
    }
    List<MinervaProject> projects = minervaProjectService.getByMinervaMachine(machine);
    for (MinervaProject minervaProject : projects) {
      minervaProjectService.delete(minervaProject);
    }
    minervaMachineService.delete(machine);
  }

  @GetMapping
  public Page<MinervaMachine> findAll(final Pageable pageable) throws QueryException {
    return minervaMachineService.findAll(pageable);
  }

}
