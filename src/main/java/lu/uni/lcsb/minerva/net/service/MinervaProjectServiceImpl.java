package lu.uni.lcsb.minerva.net.service;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Instant;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import lu.uni.lcsb.minerva.net.dao.IMinervaProjectDao;
import lu.uni.lcsb.minerva.net.dao.IProvenanceDao;
import lu.uni.lcsb.minerva.net.entity.MinervaMachine;
import lu.uni.lcsb.minerva.net.entity.MinervaProject;
import lu.uni.lcsb.minerva.net.entity.Provenance;
import lu.uni.lcsb.minerva.net.entity.ProvenanceActionType;
import lu.uni.lcsb.minerva.net.entity.Status;

@Service
public class MinervaProjectServiceImpl implements MinervaProjectService {

  protected Logger logger = LogManager.getLogger();

  private IMinervaProjectDao minervaProjectDao;
  private IProvenanceDao provenanceDao;

  private ObjectMapper objectMapper;

  @Autowired
  public MinervaProjectServiceImpl(final IMinervaProjectDao minervaProjectDao, final IProvenanceDao provenanceDao, final ObjectMapper objectMapper) {
    this.minervaProjectDao = minervaProjectDao;
    this.provenanceDao = provenanceDao;
    this.objectMapper = objectMapper;
  }

  @Override
  public MinervaProject save(final MinervaProject project) {
    MinervaProject result = minervaProjectDao.save(project);
    String message = String.format("'%s' map on machine '%s' was registered by '%s'", result.getProjectId(), result.getMachine().getRootUrl(),
        result.getContactEmail());
    logger.info(message);
    Provenance provenance = new Provenance();
    provenance.setDescription(message);
    provenance.setObjectId(project.getId());
    provenance.setObjectType(project.getClass());
    provenance.setType(ProvenanceActionType.CREATE);

    provenanceDao.save(provenance);
    return result;
  }

  @Override
  public void fillFromRemote(final MinervaProject project) throws ServiceNotAvailableException {
    var httpClient = HttpClient.newHttpClient();
    String url = String.format("%s/api/projects/%s/", project.getMachine().getRootUrl(), project.getProjectId());
    var request = HttpRequest.newBuilder()
        .header("Accept", "application/json")
        .uri(URI.create(url))
        .GET()
        .build();
    try {
      var response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
      if (response.statusCode() != HttpStatus.OK.value()) {
        if (response.statusCode() == HttpStatus.FORBIDDEN.value() || response.statusCode() == HttpStatus.NOT_FOUND.value()) {
          project.setStatus(Status.NOT_AVAILABLE);
          project.setStatusUpdatedAt(Instant.now());
        }
        throw new ServiceNotAvailableException("Problem with fetching information from " + url + ". STATUS_CODE=" + response.statusCode());
      }
      Map<String, Object> projectConfiguration = objectMapper.readValue(response.body(), new TypeReference<Map<String, Object>>() {
      });
      project.setDisease(getDiseaseFromConfiguration(projectConfiguration));
      project.setOrganism(getOrganismFromConfiguration(projectConfiguration));
      project.setMapVersion(getVersionFromConfiguration(projectConfiguration));
      project.setMapName(getNameFromConfiguration(projectConfiguration));
      project.setLicense(getLicenseFromConfiguration(projectConfiguration));

      project.setStatus(Status.OK);
      project.setStatusUpdatedAt(Instant.now());

    } catch (Exception e) {
      throw new ServiceNotAvailableException("Problem with fetching information from " + url, e);
    }
  }

  @SuppressWarnings("unchecked")
  private String getLicenseFromConfiguration(final Map<String, Object> projectConfiguration) {
    String licenseName = "";
    if (projectConfiguration.get("license") != null) {
      Map<String, Object> license = (Map<String, Object>) projectConfiguration.get("license");
      licenseName = license.get("name") + "";
    }
    return licenseName;
  }

  private String getNameFromConfiguration(final Map<String, Object> projectConfiguration) {
    String result = "";
    Object value = projectConfiguration.get("name");
    if (value instanceof String) {
      result = (String) value;
    }
    return result;
  }

  private String getVersionFromConfiguration(final Map<String, Object> projectConfiguration) {
    String result = "";
    Object value = projectConfiguration.get("version");
    if (value instanceof String) {
      result = (String) value;
    }
    return result;
  }

  private String getOrganismFromConfiguration(final Map<String, Object> projectConfiguration) {
    String result = "";
    Object disease = projectConfiguration.get("organism");
    if (disease instanceof Map) {
      Object value = ((Map<?, ?>) disease).get("resource");
      if (value instanceof String) {
        result = (String) value;
      }
    }
    return result;
  }

  private String getDiseaseFromConfiguration(final Map<String, Object> projectConfiguration) {
    String result = "";
    Object disease = projectConfiguration.get("disease");
    if (disease instanceof Map) {
      Object value = ((Map<?, ?>) disease).get("resource");
      if (value instanceof String) {
        result = (String) value;
      }
    }
    return result;
  }

  @Override
  public void deleteAll() {
    minervaProjectDao.deleteAll();
  }

  @Override
  public long count() {
    return minervaProjectDao.count();
  }

  @Override
  public MinervaProject getByProjectId(final MinervaMachine machine, final String projectId) {
    List<MinervaProject> result = minervaProjectDao.getByProjectIdAndMachine(projectId, machine);
    if (result.size() > 0) {
      return result.get(0);
    }
    return null;
  }

  @Override
  public void delete(final MinervaProject project) {
    minervaProjectDao.delete(project);
    String message = String.format("'%s' map on machine '%s' was deleted", project.getProjectId(), project.getMachine().getRootUrl());
    logger.info(message);
    Provenance provenance = new Provenance();
    provenance.setDescription(message);
    provenance.setObjectId(project.getId());
    provenance.setObjectType(project.getClass());
    provenance.setType(ProvenanceActionType.DELETE);

    provenanceDao.save(provenance);
  }

  @Override
  public Page<MinervaProject> findAll(final Pageable pageable) {
    return minervaProjectDao.findAll(pageable);
  }

  @Override
  public List<MinervaProject> getByMinervaMachine(final MinervaMachine machine) {
    return minervaProjectDao.getByMachine(machine);
  }

  @Override
  public Page<MinervaProject> findAllByMinervaMachine(final MinervaMachine machine, final Pageable pageable) {
    return minervaProjectDao.findAllByMachine(machine, pageable);
  }
}
