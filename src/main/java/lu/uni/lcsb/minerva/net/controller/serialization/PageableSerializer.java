package lu.uni.lcsb.minerva.net.controller.serialization;

import java.io.IOException;

import org.springframework.boot.jackson.JsonComponent;
import org.springframework.data.domain.Pageable;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

@JsonComponent
public class PageableSerializer extends StdSerializer<Pageable> {
  public PageableSerializer() {
    super(Pageable.class);
  }

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  @Override
  public void serialize(final Pageable value, final JsonGenerator gen, final SerializerProvider serializers)
      throws IOException {
    gen.writeStartObject();
    gen.writeNumberField("pageSize", value.getPageSize());
    gen.writeNumberField("pageNumber", value.getPageNumber());
    gen.writeObjectField("sort", value.getSort());
    gen.writeEndObject();
  }
}