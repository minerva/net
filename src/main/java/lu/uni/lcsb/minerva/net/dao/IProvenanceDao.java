package lu.uni.lcsb.minerva.net.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import lu.uni.lcsb.minerva.net.entity.Provenance;
import lu.uni.lcsb.minerva.net.entity.ProvenanceActionType;

public interface IProvenanceDao extends JpaRepository<Provenance, Integer> {

  List<Provenance> getByType(ProvenanceActionType delete);

  List<Provenance> getByObjectType(Class<?> objectType);
}
