package lu.uni.lcsb.minerva.net.service;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.security.SecureRandom;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.validation.constraints.NotNull;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import lu.uni.lcsb.minerva.net.NotImplementedException;
import lu.uni.lcsb.minerva.net.dao.IMinervaMachineDao;
import lu.uni.lcsb.minerva.net.dao.IProvenanceDao;
import lu.uni.lcsb.minerva.net.entity.MinervaMachine;
import lu.uni.lcsb.minerva.net.entity.Provenance;
import lu.uni.lcsb.minerva.net.entity.ProvenanceActionType;
import lu.uni.lcsb.minerva.net.entity.Status;

@Service
public class MinervaMachineServiceImpl implements MinervaMachineService {

  protected Logger logger = LogManager.getLogger();

  private IMinervaMachineDao minervaMachineDao;
  private IProvenanceDao provenanceDao;

  private ObjectMapper objectMapper;

  @Autowired
  public MinervaMachineServiceImpl(final IMinervaMachineDao minervaMachineDao, final ObjectMapper objectMapper, final IProvenanceDao provenanceDao) {
    this.minervaMachineDao = minervaMachineDao;
    this.objectMapper = objectMapper;
    this.provenanceDao = provenanceDao;
  }

  @Override
  public MinervaMachine update(final MinervaMachine machine) {
    return minervaMachineDao.save(machine);
  }

  @Override
  public MinervaMachine add(final MinervaMachine machine) {
    machine.setAuthenticationToken(createRandomAuthToken());
    MinervaMachine result = minervaMachineDao.save(machine);

    String message = String.format("'%s' was registered by '%s'", result.getRootUrl(), result.getName());
    logger.info(message);
    Provenance provenance = new Provenance();
    provenance.setDescription(message);
    provenance.setObjectId(machine.getId());
    provenance.setObjectType(machine.getClass());
    provenance.setType(ProvenanceActionType.CREATE);

    provenanceDao.save(provenance);

    return result;
  }

  private String createRandomAuthToken() {
    String chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    SecureRandom rnd = new SecureRandom();

    StringBuilder sb = new StringBuilder(20);
    for (int i = 0; i < 20; i++) {
      sb.append(chars.charAt(rnd.nextInt(chars.length())));
    }
    return sb.toString();
  }

  @Override
  public void checkRemoteAccess(final MinervaMachine machine) {
    throw new NotImplementedException();
  }

  @Override
  public void fillFromRemote(final MinervaMachine machine) throws ServiceNotAvailableException {
    var httpClient = HttpClient.newHttpClient();
    try {
      var request = HttpRequest.newBuilder()
          .header("Accept", "application/json")
          .uri(URI.create(machine.getRootUrl() + "/api/configuration/"))
          .GET()
          .build();
      var response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
      if (response.statusCode() != HttpStatus.OK.value()) {
        throw new ServiceNotAvailableException();
      }
      Map<String, Object> configuration = objectMapper.readValue(response.body(), new TypeReference<Map<String, Object>>() {
      });

      String version = extractVersionFromConfiguration(configuration);
      machine.setVersion(version);

      Object options = configuration.get("options");
      String url = null;
      if (options instanceof List) {
        for (Object option : (List<?>) options) {
          if (option instanceof Map) {
            Object type = ((Map<?, ?>) option).get("type");
            if (Objects.equals(type, "MINERVA_ROOT")) {
              Object value = ((Map<?, ?>) option).get("value");
              if (value instanceof String) {
                url = (String) value;
              }
            }
          } else {
            throw new ServiceNotAvailableException("Minerva API returned invalid option" + option);
          }
        }
      } else {
        throw new ServiceNotAvailableException("Minerva API returned invalid option list" + options);
      }

      if (!Objects.equals(machine.getRootUrl(), url)) {
        throw new ServiceNotAvailableException(
            String.format("Minerva API returned invalid ROOT_URL. Expected '%s' but got '%s'", machine.getRootUrl(), url));
      }

      machine.setStatus(Status.OK);
      machine.setStatusUpdatedAt(Instant.now());

    } catch (Exception e) {
      throw new ServiceNotAvailableException(e);
    }

  }

  private String extractVersionFromConfiguration(final Map<String, Object> configuration) throws ServiceNotAvailableException {
    String version;
    Object object = configuration.get("version");
    if (object instanceof String) {
      version = (String) object;
    } else {
      throw new ServiceNotAvailableException("Minerva API returned invalid version: " + object);
    }
    return version;
  }

  @Override
  public void deleteAll() {
    this.minervaMachineDao.deleteAll();
  }

  @Override
  public MinervaMachine getByUrl(final String rootUrl) {
    List<MinervaMachine> list = minervaMachineDao.getByRootUrl(rootUrl);
    if (list.size() > 0) {
      return list.get(0);
    }
    return null;
  }

  @Override
  public long count() {
    return 0;
  }

  @Override
  public MinervaMachine getById(@NotNull final int id) {
    return minervaMachineDao.findById(id).orElse(null);
  }

  @Override
  public void delete(final MinervaMachine machine) {
    minervaMachineDao.delete(machine);

    String message = String.format("'%s' was deleted", machine.getRootUrl(), machine);
    logger.info(message);
    Provenance provenance = new Provenance();
    provenance.setDescription(message);
    provenance.setObjectId(machine.getId());
    provenance.setObjectType(machine.getClass());
    provenance.setType(ProvenanceActionType.DELETE);

    provenanceDao.save(provenance);

  }

  @Override
  public Page<MinervaMachine> findAll(final Pageable pageable) {
    return minervaMachineDao.findAll(pageable);
  }

}
