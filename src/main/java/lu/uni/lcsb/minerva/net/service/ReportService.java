package lu.uni.lcsb.minerva.net.service;

import lu.uni.lcsb.minerva.net.entity.ErrorReport;

public interface ReportService {

  void saveReport(final ErrorReport report);

  void sendEmail(final ErrorReport report);

  void deleteAll();
}
