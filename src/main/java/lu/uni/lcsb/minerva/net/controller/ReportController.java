package lu.uni.lcsb.minerva.net.controller;

import java.io.IOException;
import java.time.Instant;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import lu.uni.lcsb.minerva.net.entity.ErrorReport;
import lu.uni.lcsb.minerva.net.service.GitlabService;
import lu.uni.lcsb.minerva.net.service.ReportService;

@RestController
@Validated
// "/issues" is used in minerva versions 13.1-16.0
@RequestMapping(value = { "/issues", "/api/reports" })
public class ReportController {

  private Logger logger = LogManager.getLogger();

  private ReportService reportService;
  private GitlabService gitlabService;

  @Autowired
  public ReportController(final ReportService reportService, final GitlabService gitlabService) {
    this.reportService = reportService;
    this.gitlabService = gitlabService;
  }

  @PostMapping
  public ErrorReport reportError(@Valid @RequestBody final ErrorReport report) throws IOException, InterruptedException {
    report.setId(0); // ignore potential ID in body
    reportService.saveReport(report);
    // reportService.sendEmail(report);
    gitlabService.sendReport(report);
    return report;
  }

  @ExceptionHandler
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  public ReportErrorResponse handleException(final Exception ex) {
    logger.warn(ex, ex);
    return new ReportErrorResponse("Report could not be submitted!", ex.getMessage(), Instant.now().toString());
  }

}
