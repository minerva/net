package lu.uni.lcsb.minerva.net.configuration;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.RequestMatcher;

public class AuthenticationFilter extends AbstractAuthenticationProcessingFilter {

  AuthenticationFilter(final RequestMatcher protectedUrls) {
    super(protectedUrls);
  }

  @Override
  public Authentication attemptAuthentication(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse) {
    // Authorization: BearerTOKEN
    String token = httpServletRequest.getHeader(HttpHeaders.AUTHORIZATION);
    if (token == null) {
      token = "";
    }
    token = StringUtils.removeStart(token, "Bearer").trim();
    Authentication requestAuthentication = new UsernamePasswordAuthenticationToken(token, token);
    return getAuthenticationManager().authenticate(requestAuthentication);

  }

  @Override
  protected void successfulAuthentication(final HttpServletRequest request, final HttpServletResponse response, final FilterChain chain,
      final Authentication authResult) throws IOException, ServletException {
    SecurityContextHolder.getContext().setAuthentication(authResult);
    chain.doFilter(request, response);
  }
}