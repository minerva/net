package lu.uni.lcsb.minerva.net.controller.serialization;

import java.io.IOException;
import java.time.Instant;
import java.time.format.DateTimeFormatter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.jackson.JsonComponent;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

@JsonComponent
public class InstantDeserializer extends StdDeserializer<Instant> {

  public InstantDeserializer() {
    super(Instant.class);
  }

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  @Override
  public Instant deserialize(final JsonParser jp, final DeserializationContext ctxt)
      throws IOException, JsonProcessingException {
    JsonNode node = jp.getCodec().readTree(jp);
    String text = node.asText();
    if (text == null) {
      return null;
    }
    Instant result;
    if (StringUtils.isNumeric(text)) {
      result = Instant.ofEpochSecond(Long.valueOf(text));
    } else {
      result = Instant.from(DateTimeFormatter.ISO_INSTANT.parse(text));
    }
    return result;
  }
}