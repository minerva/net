async function fetchData() {
	let response = await fetch("/api/machines/?size=2000&sort=rootUrl");
	let data = await response.json();

	for (let i = 0; i < data.pageContent.length; i++) {
		let machine = data.pageContent[i];
		let machineResponse = await fetch("/api/machines/" + machine.id + "/projects/?size=2000&sort=projectId");
		let projectsData = await machineResponse.json();
		if (projectsData.pageContent.length===0) {
			let html = "<tr>";
			html += "<td><a href=\"" + machine.rootUrl + "\">" + ((machine.rootUrl.length > 35)? machine.rootUrl.substring(0,34) + "..." : machine.rootUrl) + "</a></td>";
			html += "<td></td>";
			html += "<td></td>";
			html += "<td>" + machine.status + "</td>";
			html += "<td></td>";
			html += "<td></td>";
			html += "<td></td>";
			html += "<td></td>";
			html += "<td>" + machine.statusUpdatedAt.substring(0,19) + "</td>";
			html += "</tr>";
			$("table.minerva-net-list").append(html);
		} else {
			for (let j = 0; j < projectsData.pageContent.length; j++) {
				let project = projectsData.pageContent[j];
				let url = machine.rootUrl + "?id=" + project.projectId;
				let html = "<tr>";
				html += "<td><a href=\"" + url + "\">" + ((url.length > 35) ? url.substring(0,34) + "..." : url) + "</a></td>";
				html += "<td>" + project.projectId + "</td>";
				html += "<td><a href=\"https://meshb.nlm.nih.gov/record/ui?ui=" + project.disease + "\" target=\"_blank\">" + project.disease + "</a></td>";
				html += "<td>" + project.status + "</td>";
				html += "<td>" + project.organism + "</td>";
				html += "<td>" + project.mapName + "</td>";
				html += "<td>" + project.mapVersion + "</td>";
				html += "<td>" + project.license + "</td>";
				html += "<td>" + project.statusUpdatedAt.substring(0,19) + "</td>";
				html += "</tr>";
				$("table.minerva-net-list").append(html);
			}
		}
	}
}
