SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET default_tablespace = '';

CREATE TABLE public.error_report (
    id integer NOT NULL,
    browser character varying(255),
    comment character varying(255),
    email character varying(255),
    login character varying(255),
    stacktrace text,
    "timestamp" timestamp without time zone,
    url character varying(255),
    version character varying(255)
);

CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

SELECT pg_catalog.setval('public.hibernate_sequence', 1, false);

ALTER TABLE ONLY public.error_report
    ADD CONSTRAINT error_report_pkey PRIMARY KEY (id);
