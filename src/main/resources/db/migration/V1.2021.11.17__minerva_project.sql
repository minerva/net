CREATE TABLE public.minerva_machine (
    id integer NOT NULL,
    root_url character varying(255) NOT NULL UNIQUE,
    email character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    version character varying(255) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    status character varying(255) NOT NULL,
    status_updated_at timestamp without time zone NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE public.minerva_project (
    id integer NOT NULL,
    machine_id integer NOT NULL,
    project_id character varying(255) NOT NULL,
    contact_email character varying(255) NOT NULL,
    map_name character varying(255) NOT NULL,
    map_version character varying(255) NOT NULL,
    organism character varying(255) NOT NULL,
    disease character varying(255) NOT NULL,
    status character varying(255) NOT NULL,
    status_updated_at timestamp without time zone NOT NULL,
    created_at timestamp without time zone NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (machine_id) REFERENCES minerva_machine(id),
    UNIQUE (machine_id, project_id)
);
