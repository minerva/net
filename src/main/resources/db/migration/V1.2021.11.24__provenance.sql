CREATE TABLE public.provenance (
    id integer NOT NULL,
    object_type character varying(512) NOT NULL,
    object_id integer NOT NULL,
    type character varying(255) NOT NULL,
    description text NOT NULL,
    created_at timestamp without time zone NOT NULL,
    PRIMARY KEY (id)
);
