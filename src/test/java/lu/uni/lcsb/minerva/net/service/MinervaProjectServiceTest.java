package lu.uni.lcsb.minerva.net.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import lu.uni.lcsb.minerva.net.dao.IProvenanceDao;
import lu.uni.lcsb.minerva.net.entity.MinervaProject;
import lu.uni.lcsb.minerva.net.entity.ProvenanceActionType;
import lu.uni.lcsb.minerva.net.helper.TestConfig;
import lu.uni.lcsb.minerva.net.helper.TestHelper;

@ContextConfiguration(classes = TestConfig.class)
@SpringBootTest
class MinervaProjectServiceTest {

  @Autowired
  private MinervaProjectService minervaProjectService;

  @Autowired
  private IProvenanceDao provenanceDao;

  @Autowired
  private TestHelper helper;

  @AfterEach
  public void cleanUp() {
    helper.cleanUp();
  }

  @Test
  void testAddProvenance() {
    minervaProjectService.save(helper.createProject(helper.createAndPersistMachine()));
    assertEquals(1, provenanceDao.getByObjectType(MinervaProject.class).size());
  }

  @Test
  void testDeleteProvenance() {
    MinervaProject project = helper.createAndPersistProject();

    assertEquals(0, provenanceDao.getByType(ProvenanceActionType.DELETE).size());
    minervaProjectService.delete(project);
    assertEquals(1, provenanceDao.getByType(ProvenanceActionType.DELETE).size());
  }

}
