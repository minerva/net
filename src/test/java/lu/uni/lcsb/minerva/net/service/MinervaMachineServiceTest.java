package lu.uni.lcsb.minerva.net.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import lu.uni.lcsb.minerva.net.dao.IProvenanceDao;
import lu.uni.lcsb.minerva.net.entity.MinervaMachine;
import lu.uni.lcsb.minerva.net.entity.ProvenanceActionType;
import lu.uni.lcsb.minerva.net.helper.TestConfig;
import lu.uni.lcsb.minerva.net.helper.TestHelper;

@ContextConfiguration(classes = TestConfig.class)
@SpringBootTest
class MinervaMachineServiceTest {

  @Autowired
  private MinervaMachineService minervaMachineService;

  @Autowired
  private IProvenanceDao provenanceDao;

  @Autowired
  private TestHelper helper;

  @AfterEach
  public void cleanUp() {
    helper.cleanUp();
  }

  @Test
  void testAddProvenance() {
    minervaMachineService.add(helper.createMachine());
    assertEquals(1, provenanceDao.count());
  }

  @Test
  void testUpdate() {
    MinervaMachine machine = minervaMachineService.add(helper.createMachine());
    String token = machine.getAuthenticationToken();
    minervaMachineService.update(machine);
    assertEquals(token, minervaMachineService.getById(machine.getId()).getAuthenticationToken());
  }

  @Test
  void testDeleteProvenance() {
    MinervaMachine machine = minervaMachineService.add(helper.createMachine());
    assertEquals(0, provenanceDao.getByType(ProvenanceActionType.DELETE).size());
    minervaMachineService.delete(machine);
    assertEquals(1, provenanceDao.getByType(ProvenanceActionType.DELETE).size());
  }

  @Test
  void testFillFromInvalidRemote() {
    MinervaMachine machine = helper.createMachine();
    machine.setRootUrl("invalid");
    assertThrows(ServiceNotAvailableException.class, () -> {
      minervaMachineService.fillFromRemote(machine);
    });
  }

}
