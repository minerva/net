package lu.uni.lcsb.minerva.net.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Map;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

class GitlabReportTest {

  @Test
  void testJsonSerialization() throws Exception {
    GitlabReport report = new GitlabReport("tit", "desc");
    System.out.println(report.toJSON());
    Map<String, Object> result = new ObjectMapper().readValue(report.toJSON(),
        new TypeReference<Map<String, Object>>() {
        });
    assertEquals("tit", result.get("title"));
    assertEquals("desc", result.get("description"));
    assertEquals("true", result.get("confidential"));
    assertEquals("BUG", result.get("labels"));
  }

}
