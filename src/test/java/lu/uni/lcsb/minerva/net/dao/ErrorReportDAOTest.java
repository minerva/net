package lu.uni.lcsb.minerva.net.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import lu.uni.lcsb.minerva.net.entity.ErrorReport;
import lu.uni.lcsb.minerva.net.helper.TestConfig;

@ContextConfiguration(classes = TestConfig.class)
@SpringBootTest
@Transactional
class ErrorReportDAOTest {

  @Autowired
  private IErrorReportDao erroReportDao;

  @Test
  void testGetReports() {
    ErrorReport testReport = new ErrorReport();
    testReport.setVersion("13.0.0");
    testReport.setStacktrace("SC");
    erroReportDao.save(testReport);
    assertEquals(1, erroReportDao.findAll().size());
  }

}
