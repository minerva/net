package lu.uni.lcsb.minerva.net;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

import lu.uni.lcsb.minerva.net.service.GitlabService;

@Profile("gitlab_mock")
@Configuration
public class GitlabMockTestConfiguration {
  @Bean
  @Primary
  public GitlabService gitlabService() {
    return Mockito.mock(GitlabService.class);
  }
}
