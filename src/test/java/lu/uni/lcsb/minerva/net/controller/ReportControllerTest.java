package lu.uni.lcsb.minerva.net.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.Instant;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import lu.uni.lcsb.minerva.net.entity.ErrorReport;

@ActiveProfiles("gitlab_mock")
class ReportControllerTest extends AControllerTest {

  private static final String SAMPLE_JS_STACKTRACE = "Error: Failed to fetch\n"
      + "at new NetworkError (https://www.vmh.life/minerva/javax.faces.resource/minerva.js.xhtml?ln=js&m_version=2fff2388d062df936da940ccc9a19182b08e9e86:260356:17)\n"
      + "at Request._callback (https://www.vmh.life/minerva/javax.faces.resource/minerva.js.xhtml?ln=js&m_version=2fff2388d062df936da940ccc9a19182b08e9e86:260859:16)\n"
      + "at self.callback (https://www.vmh.life/minerva/javax.faces.resource/minerva.js.xhtml?ln=js&m_version=2fff2388d062df936da940ccc9a19182b08e9e86:208854:22)\n"
      + "at Request.EventEmitter.emit (https://www.vmh.life/minerva/javax.faces.resource/minerva.js.xhtml?ln=js&m_version=2fff2388d062df936da940ccc9a19182b08e9e86:30805:17)\n"
      + "at Request.onRequestError (https://www.vmh.life/minerva/javax.faces.resource/minerva.js.xhtml?ln=js&m_version=2fff2388d062df936da940ccc9a19182b08e9e86:209550:8)\n"
      + "at module.exports.EventEmitter.emit (https://www.vmh.life/minerva/javax.faces.resource/minerva.js.xhtml?ln=js&m_version=2fff2388d062df936da940ccc9a19182b08e9e86:30805:17)\n"
      + "at https://www.vmh.life/minerva/javax.faces.resource/minerva.js.xhtml?ln=js&m_version=2fff2388d062df936da940ccc9a19182b08e9e86:219418:9\n"
      + "From previous event:\n"
      + "at ObjectWithListeners.ServerConnector._sendRequest (https://www.vmh.life/minerva/javax.faces.resource/minerva.js.xhtml?ln=js&m_version=2fff2388d062df936da940ccc9a19182b08e9e86:260856:10)\n"
      + "at https://www.vmh.life/minerva/javax.faces.resource/minerva.js.xhtml?ln=js&m_version=2fff2388d062df936da940ccc9a19182b08e9e86:260825:17\n"
      + "From previous event:\n"
      + "at ObjectWithListeners.ServerConnector.sendRequest (https://www.vmh.life/minerva/javax.faces.resource/minerva.js.xhtml?ln=js&m_version=2fff2388d062df936da940ccc9a19182b08e9e86:260824:61)\n"
      + "at ObjectWithListeners.ServerConnector.sendGetRequest (https://www.vmh.life/minerva/javax.faces.resource/minerva.js.xhtml?ln=js&m_version=2fff2388d062df936da940ccc9a19182b08e9e86:260781:15)\n"
      + "at ObjectWithListeners.ServerConnector.getUser (https://www.vmh.life/minerva/javax.faces.resource/minerva.js.xhtml?ln=js&m_version=2fff2388d062df936da940ccc9a19182b08e9e86:261921:15)\n"
      + "at ObjectWithListeners.ServerConnector.getLoggedUser (https://www.vmh.life/minerva/javax.faces.resource/minerva.js.xhtml?ln=js&m_version=2fff2388d062df936da940ccc9a19182b08e9e86:261867:29)\n"
      + "at https://www.vmh.life/minerva/javax.faces.resource/minerva.js.xhtml?ln=js&m_version=2fff2388d062df936da940ccc9a19182b08e9e86:261698:21\n"
      + "From previous event:\n"
      + "at https://www.vmh.life/minerva/javax.faces.resource/minerva.js.xhtml?ln=js&m_version=2fff2388d062df936da940ccc9a19182b08e9e86:261696:10\n"
      + "From previous event:\n"
      + "at ObjectWithListeners.ServerConnector.getProject (https://www.vmh.life/minerva/javax.faces.resource/minerva.js.xhtml?ln=js&m_version=2fff2388d062df936da940ccc9a19182b08e9e86:261679:6)\n"
      + "at getProject (https://www.vmh.life/minerva/javax.faces.resource/minerva.js.xhtml?ln=js&m_version=2fff2388d062df936da940ccc9a19182b08e9e86:300228:28)\n"
      + "at https://www.vmh.life/minerva/javax.faces.resource/minerva.js.xhtml?ln=js&m_version=2fff2388d062df936da940ccc9a19182b08e9e86:300395:12\n"
      + "From previous event:\n"
      + "at Object.create (https://www.vmh.life/minerva/javax.faces.resource/minerva.js.xhtml?ln=js&m_version=2fff2388d062df936da940ccc9a19182b08e9e86:300393:6)\n"
      + "at initMap (https://www.vmh.life/minerva/index.xhtml?id=ReconMap-3&search=gthrd[m]&drugSearch=Glutathione&x=34209&y=30126&zoom=8&:41:20)\n"
      + "at onload (https://www.vmh.life/minerva/index.xhtml?id=ReconMap-3&search=gthrd[m]&drugSearch=Glutathione&x=34209&y=30126&zoom=8&:52:45)";

  private static final String SAMPLE_JAVA_STACKTRACE = "java.lang.Exception\n"
      + "    at lu.uni.lcsb.minerva.net.controller.ReportControllerTest.createReportWithJavaStacktrace(ReportControllerTest.java:190)\n"
      + "    at java.base/jdk.internal.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\n"
      + "    at java.base/jdk.internal.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\n"
      + "    at java.base/jdk.internal.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\n"
      + "    at java.base/java.lang.reflect.Method.invoke(Method.java:566)\n"
      + "";
  @Autowired
  protected ObjectMapper objectMapper;

  @Test
  void createReport() throws Exception {
    ErrorReport testReport = new ErrorReport();
    testReport.setVersion("13.0.0");
    testReport.setTimestamp(Instant.now());
    testReport.setStacktrace(SAMPLE_JS_STACKTRACE);

    var request = post("/api/reports")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(testReport));

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document(
            "createReport",
            pathParameters(),
            requestParameters(),
            Docs.getReportControllerResponse()))
        .andReturn().getResponse().getContentAsString();

    ErrorReport report = objectMapper.readValue(response, new TypeReference<ErrorReport>() {
    });
    assertTrue(report.getId() > 0);
  }

  @Test
  void createReportWith() throws Exception {
    ErrorReport testReport = new ErrorReport();
    testReport.setVersion("13.0.0");
    testReport.setTimestamp(Instant.now());
    testReport.setStacktrace(SAMPLE_JS_STACKTRACE);

    var request = post("/api/reports")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(testReport));

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document(
            "createReport",
            pathParameters(),
            requestParameters(),
            Docs.getReportControllerResponse()))
        .andReturn().getResponse().getContentAsString();

    ErrorReport report = objectMapper.readValue(response, new TypeReference<ErrorReport>() {
    });
    assertTrue(report.getId() > 0);
  }

  @Test
  void createReportFromLongTimestamp() throws Exception {
    ErrorReport testReport = new ErrorReport();
    testReport.setVersion("13.0.0");
    testReport.setTimestamp(Instant.now());
    testReport.setStacktrace(SAMPLE_JS_STACKTRACE);
    Map<String, Object> map = objectMapper.convertValue(testReport, new TypeReference<Map<String, Object>>() {
    });
    map.put("timestamp", "1641995945");

    var request = post("/api/reports")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(map));

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document(
            "createReport",
            pathParameters(),
            requestParameters(),
            Docs.getReportControllerResponse()))
        .andReturn().getResponse().getContentAsString();

    ErrorReport report = objectMapper.readValue(response, new TypeReference<ErrorReport>() {
    });
    assertTrue(report.getId() > 0);
  }

  @Test
  void createMinimalReport() throws Exception {
    ErrorReport testReport = new ErrorReport();
    testReport.setVersion("13.0.0");
    testReport.setStacktrace(SAMPLE_JS_STACKTRACE);
    testReport.setId(-1);

    var request = post("/api/reports")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(testReport));

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    ErrorReport report = objectMapper.readValue(response, new TypeReference<ErrorReport>() {
    });
    assertTrue(report.getId() > 0);
  }

  @Test
  void createInvalidReport() throws Exception {
    ErrorReport testReport = new ErrorReport();
    testReport.setId(-1);

    var request = post("/api/reports")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(testReport));

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  void createReportWithJavaStacktrace() throws Exception {
    ErrorReport testReport = new ErrorReport();
    testReport.setVersion("13.0.0");
    testReport.setStacktrace(SAMPLE_JS_STACKTRACE);
    testReport.setJavaStacktrace(SAMPLE_JAVA_STACKTRACE);
    testReport.setId(-1);

    var request = post("/api/reports")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(testReport));

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    ErrorReport report = objectMapper.readValue(response, new TypeReference<ErrorReport>() {
    });
    assertTrue(report.getId() > 0);

    assertEquals(SAMPLE_JAVA_STACKTRACE, report.getJavaStacktrace());
  }

}
