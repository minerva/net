package lu.uni.lcsb.minerva.net.controller;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;

import javax.servlet.Filter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import lu.uni.lcsb.minerva.net.UnitTestFailedWatcher;
import lu.uni.lcsb.minerva.net.helper.TestHelper;

@SpringBootTest
@ExtendWith({ RestDocumentationExtension.class, SpringExtension.class, UnitTestFailedWatcher.class })
public abstract class AControllerTest {

  protected Logger logger = LogManager.getLogger();
  protected MockMvc mockMvc;

  @Autowired
  private WebApplicationContext context;

  @Autowired
  private Filter springSecurityFilterChain;

  @Autowired
  protected TestHelper helper;

  @AfterEach
  public void cleanUp() {
    helper.cleanUp();
  }

  @BeforeEach
  public void construct(final RestDocumentationContextProvider restDocumentation) {
    var restDocConfig = documentationConfiguration(restDocumentation)
        .uris()
        .withHost("minerva-net.lcsb.uni.lu")
        .withPort(443)
        .withScheme("https")
        .and()
        .operationPreprocessors()
        .withResponseDefaults(
            prettyPrint());
    mockMvc = MockMvcBuilders.webAppContextSetup(context)
        .apply(restDocConfig)
        .addFilter(springSecurityFilterChain)
        .build();
  }

}
