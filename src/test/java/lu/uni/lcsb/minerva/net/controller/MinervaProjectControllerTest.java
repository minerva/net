package lu.uni.lcsb.minerva.net.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;

import com.fasterxml.jackson.databind.ObjectMapper;

import lu.uni.lcsb.minerva.net.controller.MinervaProjectController.MinervaProjectPostData;
import lu.uni.lcsb.minerva.net.entity.MinervaMachine;
import lu.uni.lcsb.minerva.net.entity.MinervaProject;
import lu.uni.lcsb.minerva.net.helper.TestHelper;
import lu.uni.lcsb.minerva.net.service.MinervaMachineService;
import lu.uni.lcsb.minerva.net.service.MinervaProjectService;

@ActiveProfiles("gitlab_mock")
class MinervaProjectControllerTest extends AControllerTest {

  @Autowired
  protected ObjectMapper objectMapper;

  @Autowired
  protected MinervaMachineService minervaMachineService;

  @Autowired
  protected MinervaProjectService minervaProjectService;

  @Test
  void testCreateProjectNoAuthentication() throws Exception {
    MinervaMachine machine = helper.createAndPersistMachine();

    MinervaProjectPostData data = new MinervaProjectPostData();
    data.setEmail(TestHelper.VALID_EMAIL);
    data.setProjectId(TestHelper.VALID_PROJECT_ID);

    var request = post("/api/machines/{id}/projects/", machine.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data));

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  void testCreateProject() throws Exception {
    MinervaMachine machine = helper.createAndPersistMachine();

    MinervaProjectPostData data = new MinervaProjectPostData();
    data.setEmail(TestHelper.VALID_EMAIL);
    data.setProjectId(TestHelper.VALID_PROJECT_ID);

    var request = post("/api/machines/{id}/projects/", machine.getId())
        .header(HttpHeaders.AUTHORIZATION, "Bearer " + machine.getAuthenticationToken())
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data));

    mockMvc.perform(request)
        .andDo(document(
            "createProject",
            Docs.getMachinePathParameters(),
            Docs.createMachineControllerRequest(),
            Docs.getMachineControllerResponse()))
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    assertEquals(1, minervaProjectService.count());
  }

  @Test
  void createDuplicate() throws Exception {
    MinervaMachine machine = helper.createAndPersistMachine();

    MinervaProjectPostData data = new MinervaProjectPostData();
    data.setEmail(TestHelper.VALID_EMAIL);
    data.setProjectId(TestHelper.VALID_PROJECT_ID);

    var request = post("/api/machines/{id}/projects/", machine.getId())
        .header(HttpHeaders.AUTHORIZATION, "Bearer " + machine.getAuthenticationToken())
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data));

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    mockMvc.perform(request)
        .andExpect(status().isConflict());

    assertEquals(1, minervaProjectService.count());
  }

  @Test
  void testCreateInvalidProject() throws Exception {
    MinervaMachine machine = helper.createAndPersistMachine();

    MinervaProjectPostData data = new MinervaProjectPostData();
    data.setEmail(TestHelper.VALID_EMAIL);
    data.setProjectId(TestHelper.INVALID_PROJECT_ID);

    var request = post("/api/machines/{id}/projects/", machine.getId())
        .header(HttpHeaders.AUTHORIZATION, "Bearer " + machine.getAuthenticationToken())
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data));

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());

    assertEquals(0, minervaProjectService.count());
  }

  @Test
  void deleteProject() throws Exception {
    MinervaProject project = helper.createAndPersistProject();

    var request = delete("/api/machines/{machineId}/projects/{projectId}", project.getMachine().getId(), project.getProjectId())
        .header(HttpHeaders.AUTHORIZATION, "Bearer " + project.getMachine().getAuthenticationToken());

    mockMvc.perform(request)
        .andDo(document(
            "deleteProject",
            Docs.getProjectPathParameters()))
        .andExpect(status().is2xxSuccessful());

    assertEquals(0, minervaProjectService.count());
  }

  @Test
  void deleteProjectNoAccess() throws Exception {
    MinervaProject project = helper.createAndPersistProject();

    var request = delete("/api/machines/{id}/projects/{projectId}", project.getMachine().getId(), project.getProjectId());

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  void deleteInvalidProject() throws Exception {
    MinervaProject project = helper.createAndPersistProject();

    var request = delete("/api/machines/{id}/projects/{projectId}", project.getMachine().getId(), "bla")
        .header(HttpHeaders.AUTHORIZATION, "Bearer " + project.getMachine().getAuthenticationToken());

    mockMvc.perform(request)
        .andExpect(status().isNotFound());

    assertEquals(1, minervaProjectService.count());
  }

  @Test
  void deleteInvalidProjectMachine() throws Exception {
    MinervaProject project = helper.createAndPersistProject();

    var request = delete("/api/machines/{id}/projects/{projectId}", -1, project.getProjectId());

    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());

    assertEquals(1, minervaProjectService.count());
  }

  @Test
  void listProjects() throws Exception {
    MinervaProject project = helper.createAndPersistProject();

    var request = get("/api/machines/{id}/projects/", project.getMachine().getId());

    mockMvc.perform(request)
        .andDo(document(
            "listProjects",
            Docs.getMachinePathParameters(),
            Docs.getListMachinesControllerResponse()))
        .andExpect(status().is2xxSuccessful());
  }

}
