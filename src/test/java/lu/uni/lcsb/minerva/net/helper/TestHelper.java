package lu.uni.lcsb.minerva.net.helper;

import java.time.Instant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lu.uni.lcsb.minerva.net.entity.MinervaMachine;
import lu.uni.lcsb.minerva.net.entity.MinervaProject;
import lu.uni.lcsb.minerva.net.entity.Status;
import lu.uni.lcsb.minerva.net.service.MinervaMachineService;
import lu.uni.lcsb.minerva.net.service.MinervaProjectService;
import lu.uni.lcsb.minerva.net.service.ProvenanceService;
import lu.uni.lcsb.minerva.net.service.ReportService;

@Component
public class TestHelper {
  public static final String VALID_MACHINE_URL = "https://pdmap.uni.lu/minerva/";
  public static final String VALID_EMAIL = "minerva@uni.lu";
  public static final String VALID_PROJECT_ID = "pdmap";
  public static final String INVALID_PROJECT_ID = "unkown-map-123";

  @Autowired
  private ReportService reportService;

  @Autowired
  private MinervaMachineService minervaMachineService;

  @Autowired
  private MinervaProjectService minervaProjectService;

  @Autowired
  private ProvenanceService provenanceService;

  public MinervaMachine createMachine() {
    MinervaMachine machine = new MinervaMachine();
    machine.setEmail(VALID_EMAIL);
    machine.setName("John Doe");
    machine.setRootUrl(VALID_MACHINE_URL);
    machine.setStatus(Status.OK);
    machine.setStatusUpdatedAt(Instant.now());
    machine.setVersion("0.0.1");
    return machine;
  }

  public MinervaMachine createAndPersistMachine() {
    return minervaMachineService.add(createMachine());
  }

  public MinervaProject createAndPersistProject(final MinervaMachine machine) {
    MinervaProject project = createProject(machine);
    return minervaProjectService.save(project);
  }

  public MinervaProject createAndPersistProject() {
    MinervaMachine machine = createAndPersistMachine();
    return createAndPersistProject(machine);
  }

  public void cleanUp() {
    reportService.deleteAll();
    minervaProjectService.deleteAll();
    minervaMachineService.deleteAll();
    provenanceService.deleteAll();
  }

  public MinervaProject createProject(final MinervaMachine machine) {
    MinervaProject project = new MinervaProject();

    project.setMachine(machine);
    project.setProjectId(VALID_PROJECT_ID);
    project.setContactEmail(TestHelper.VALID_EMAIL);
    project.setMapName("x");
    project.setMapVersion("0.0.1");
    project.setStatus(Status.OK);
    project.setStatusUpdatedAt(Instant.now());
    project.setOrganism("");
    project.setLicense("CC0 1.0 Universal");
    project.setDisease("");
    return project;
  }

}
