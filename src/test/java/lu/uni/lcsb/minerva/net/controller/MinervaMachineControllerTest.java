package lu.uni.lcsb.minerva.net.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import lu.uni.lcsb.minerva.net.controller.MinervaMachineController.MinervaMachinePostData;
import lu.uni.lcsb.minerva.net.entity.MinervaMachine;
import lu.uni.lcsb.minerva.net.helper.TestHelper;
import lu.uni.lcsb.minerva.net.service.MinervaMachineService;

@ActiveProfiles("gitlab_mock")
class MinervaMachineControllerTest extends AControllerTest {

  @Autowired
  protected ObjectMapper objectMapper;

  @Autowired
  protected MinervaMachineService minervaMachineService;

  @Test
  void create() throws Exception {
    MinervaMachinePostData data = new MinervaMachinePostData();
    data.setRootUrl(TestHelper.VALID_MACHINE_URL);
    data.setEmail(TestHelper.VALID_EMAIL);
    data.setName("John Doe");

    var request = post("/api/machines/")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data));

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document(
            "createMachine",
            pathParameters(),
            Docs.createProjectControllerRequest(),
            Docs.getProjectControllerResponse()))
        .andReturn().getResponse().getContentAsString();

    Map<String, Object> responseData = objectMapper.readValue(response, new TypeReference<Map<String, Object>>() {
    });
    assertNotNull(responseData.get("authenticationToken"));

    MinervaMachine machine = objectMapper.readValue(response, new TypeReference<MinervaMachine>() {
    });
    assertTrue(machine.getId() > 0);
  }

  @Test
  void list() throws Exception {
    helper.createAndPersistProject();

    var request = get("/api/machines/");

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document(
            "listMachines",
            pathParameters(),
            Docs.getListProjectsControllerResponse()));
  }

  @Test
  void preventDuplicates() throws Exception {
    MinervaMachinePostData data = new MinervaMachinePostData();
    data.setRootUrl(TestHelper.VALID_MACHINE_URL);
    data.setEmail(TestHelper.VALID_EMAIL);
    data.setName("John Doe");

    var request = post("/api/machines/")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data));

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    mockMvc.perform(request)
        .andExpect(status().isConflict());
  }

  @Test
  void deleteMachine() throws Exception {
    MinervaMachine machine = helper.createAndPersistMachine();

    var request = delete("/api/machines/{id}", machine.getId())
        .header(HttpHeaders.AUTHORIZATION, "Bearer " + machine.getAuthenticationToken());

    mockMvc.perform(request)
        .andDo(document(
            "deleteMachine",
            Docs.getMachinePathParameters()))
        .andExpect(status().is2xxSuccessful());

    assertEquals(0, minervaMachineService.count());
  }

  @Test
  void deleteMachineWithProject() throws Exception {
    MinervaMachine machine = helper.createAndPersistMachine();
    helper.createAndPersistProject(machine);

    var request = delete("/api/machines/{id}", machine.getId())
        .header(HttpHeaders.AUTHORIZATION, "Bearer " + machine.getAuthenticationToken());

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    assertEquals(0, minervaMachineService.count());
  }

  @Test
  void deleteNonExistingMachine() throws Exception {
    var request = delete("/api/machines/{id}", -1);

    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());
  }

  @Test
  void createMachineNoEmail() throws Exception {
    MinervaMachinePostData data = new MinervaMachinePostData();
    data.setRootUrl(TestHelper.VALID_MACHINE_URL);
    data.setName("bla");

    var request = post("/api/machines/")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data));

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

}
