package lu.uni.lcsb.minerva.net.controller;

import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.subsectionWithPath;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;

import java.util.Arrays;
import java.util.List;

import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.restdocs.snippet.Snippet;

public class Docs {

  public static Snippet getReportControllerResponse() {
    return responseFields(
        fieldWithPath("id").description("report id").type(JsonFieldType.NUMBER),
        fieldWithPath("url").description("url where it happened").type(JsonFieldType.STRING).optional(),
        fieldWithPath("login").description("user login").type(JsonFieldType.STRING).optional(),
        fieldWithPath("email").description("contact email address").type(JsonFieldType.STRING).optional(),
        fieldWithPath("browser").description("browser").type(JsonFieldType.STRING).optional(),
        fieldWithPath("timestamp").description("timestamp").type(JsonFieldType.STRING).optional(),
        fieldWithPath("version").description("minerva version").type(JsonFieldType.STRING),
        fieldWithPath("comment").description("user comment").type(JsonFieldType.STRING).optional(),
        fieldWithPath("stacktrace").description("JavaScript stacktrace").type(JsonFieldType.STRING),
        fieldWithPath("javaStacktrace").description("Java stacktrace").type(JsonFieldType.STRING).optional());
  }

  public static Snippet getProjectControllerResponse() {
    return responseFields(projectFields());
  }

  public static Snippet createProjectControllerRequest() {
    return requestFields(
        fieldWithPath("email").description("contact email address").type(JsonFieldType.STRING),
        fieldWithPath("name").description("who is registering the machine").type(JsonFieldType.STRING),
        fieldWithPath("rootUrl").description("root url of minerva installation").type(JsonFieldType.STRING));
  }

  protected static List<FieldDescriptor> projectFields() {
    return Arrays.asList(fieldWithPath("id").description("machine id").type(JsonFieldType.NUMBER),
        fieldWithPath("version").description("minerva version").type(JsonFieldType.STRING),
        fieldWithPath("status").description("machine status").type(JsonFieldType.STRING),
        fieldWithPath("authenticationToken").description("token that should be used when modyfing machine data")
            .type(JsonFieldType.STRING)
            .optional(),
        fieldWithPath("statusUpdatedAt").description("timestamp when the status was last updated")
            .type(JsonFieldType.STRING),
        fieldWithPath("createdAt").description("timestamp when machine was registered in the net")
            .type(JsonFieldType.STRING),
        fieldWithPath("rootUrl").description("root url of minerva installation").type(JsonFieldType.STRING));
  }

  public static Snippet getListProjectsControllerResponse() {
    return getPageableWrapperPayload(false, projectFields());
  }

  public static Snippet getMachinePathParameters() {
    return pathParameters(parameterWithName("id")
        .description("machine id"));
  }

  protected static Snippet getPageableWrapperPayload(final boolean ignoreInDocumentation,
      final List<FieldDescriptor> dataPayload) {
    List<FieldDescriptor> fields = Arrays.asList(fieldWithPath("isLastPage")
        .description("is it the last page in pageable response")
        .type(JsonFieldType.BOOLEAN),
        fieldWithPath("totalPages")
            .description("number of pages")
            .type(JsonFieldType.NUMBER),
        fieldWithPath("totalElements")
            .description("number of elements in the database")
            .type(JsonFieldType.NUMBER),
        fieldWithPath("pageSize")
            .description("page size")
            .type(JsonFieldType.NUMBER),
        fieldWithPath("pageContent")
            .description("page content")
            .type(JsonFieldType.ARRAY),
        fieldWithPath("pageNumber")
            .description("page number")
            .type(JsonFieldType.NUMBER));

    if (ignoreInDocumentation) {
      for (FieldDescriptor descriptor : fields) {
        descriptor.ignored();
      }
    }

    return responseFields(fields).andWithPrefix("pageContent[].", dataPayload);

  }

  public static Snippet createMachineControllerRequest() {
    return requestFields(
        fieldWithPath("email").description("contact email address").type(JsonFieldType.STRING),
        fieldWithPath("projectId").description("project id").type(JsonFieldType.STRING));
  }

  public static Snippet getMachineControllerResponse() {
    return responseFields(machineFields());
  }

  protected static List<FieldDescriptor> machineFields() {
    return Arrays.asList(fieldWithPath("projectId").description("project id").type(JsonFieldType.STRING),
        fieldWithPath("mapVersion").description("project version").type(JsonFieldType.STRING),
        fieldWithPath("mapName").description("project name").type(JsonFieldType.STRING),
        fieldWithPath("organism").description("organism (taxonomy ID)").type(JsonFieldType.STRING),
        fieldWithPath("disease").description("disease (mesh ID)").type(JsonFieldType.STRING),
        fieldWithPath("status").description("project status").type(JsonFieldType.STRING),
        fieldWithPath("license").description("project license").type(JsonFieldType.STRING),
        fieldWithPath("statusUpdatedAt").description("timestamp when the status was last updated")
            .type(JsonFieldType.STRING),
        fieldWithPath("createdAt").description("timestamp when project was registered in the net")
            .type(JsonFieldType.STRING),
        subsectionWithPath("machine").description("machine where project is located"));
  }

  public static Snippet getProjectPathParameters() {
    return pathParameters(
        parameterWithName("machineId").description("machine id"),
        parameterWithName("projectId").description("project id"));
  }

  public static Snippet getListMachinesControllerResponse() {
    return getPageableWrapperPayload(false, machineFields());
  }

}
