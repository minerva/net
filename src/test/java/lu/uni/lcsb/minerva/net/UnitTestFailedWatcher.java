package lu.uni.lcsb.minerva.net;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestWatcher;

public class UnitTestFailedWatcher implements TestWatcher {

  @Override
  public void testFailed(final ExtensionContext extensionContext, final Throwable e) {
    if (!(e instanceof AssertionError)) {
      e.printStackTrace();
    }
  }
}
