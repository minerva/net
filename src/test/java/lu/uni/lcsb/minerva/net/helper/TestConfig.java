package lu.uni.lcsb.minerva.net.helper;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import lu.uni.lcsb.minerva.net.configuration.ServiceConfig;

@Configuration
@Import({ ServiceConfig.class })
@ComponentScan(basePackages = {
    "lu.uni.lcsb.minerva.net.helper" })
public class TestConfig {

}
