# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.3.1] - 2023-09-15
### Fixed
- default license added for old entries

## [1.3.0] - 2023-09-15
### Improvement
- minerva project can have license attached (#14)

## [1.2.0] - 2022-07-23
### Improvement
- error report allows for Java stacktrace to be provided (#12)
- error reports sent to gitlab are confidential (#11)
- error reports sent to gitlab are labeled as "BUG" (#6)


## [1.1.0] - 2022-07-23
### Improvement
- frontend is provided by accessing data over API
### Fixed
- url to map contained one extra '/' that prevented map from being shown (#10)


## [1.0.2] - 2022-01-25
### Fixed
- authentication token was uncontrollably modified on daily minerva validation

## [1.0.1] - 2022-01-25
### Fixed
- null email when adding machine resulted in Internal Server Error
- removing machine with projects resulted in Internal Server Error

## [1.0.0] - 2022-01-11
### Removed
- Support for postgres 9.6 and below is removed
### Added
- Repository of projects and machines
- REST API docs 
### Fixed
- Security issue with log4j2 <2.17.1 addressed

## [0.0.1] - 2020-06-25
### Added
- Initial implementation

